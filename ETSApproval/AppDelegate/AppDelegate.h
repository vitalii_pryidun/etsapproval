//
//  AppDelegate.h
//  ETSApproval
//
//  Created by supAdmin on 01.11.17.
//  Copyright © 2017 SAPRun. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end


//
//  BaseObject.h
//  ETSApproval
//
//  Created by supAdmin on 26.01.18.
//  Copyright © 2018 SAPRun. All rights reserved.
//

#import <Foundation/Foundation.h>

#if !defined(EmptyIfNil)
#define EmptyIfNil(A)  ({ __typeof__(A) __a = (A); __a ? __a : @""; })
#endif

#if !defined(ZeroIfNil)
#define ZeroIfNil(A)  ({ __typeof__(A) __a = (A); __a ? __a : @"0,0"; })
#endif

@protocol HHParsable

@required
- (instancetype)initWithJSON:(NSDictionary *)json;
- (NSDictionary *)JSONRepresentation;

- (instancetype)initWithArrayRepresentation:(NSArray *)arrayData;
- (NSArray *)arrayRepresentation;

- (NSString *)stringFrom:(id)utf8Data;

@end

@interface BaseObject : NSObject <HHParsable>

@end

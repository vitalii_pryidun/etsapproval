//
//  BaseObject.m
//  ETSApproval
//
//  Created by supAdmin on 26.01.18.
//  Copyright © 2018 SAPRun. All rights reserved.
//

#import "BaseObject.h"

// 'virtual' class
@implementation BaseObject

- (instancetype)initWithJSON:(NSDictionary *)json {
	return [super init];
}

- (NSDictionary *)JSONRepresentation {
	return [[NSDictionary alloc] init];
}

- (instancetype)initWithArrayRepresentation:(NSArray *)arrayData {
	return [super init];
}

- (NSArray *)arrayRepresentation {
	return [[NSArray alloc] init];
}

- (NSString *)stringFrom:(id)utf8Data {
	
	return EmptyIfNil([NSString stringWithUTF8String:[utf8Data UTF8String]]);
}

@end

//
//  DataManager.h
//  ETSApproval
//
//  Created by supAdmin on 02.11.17.
//  Copyright © 2017 SAPRun. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSInteger, EAErrorCode){
	NoError = 200,
	LoginFailure = 401,
	NotFound = 404,
	ServerError = 500,
	ServerCrash = 503,
};

#define isDEBUG 0

@interface DataManager : NSObject

+ (DataManager *)instance;

- (void)authWithLogin:(NSString *)login password:(NSString *)password success:(void (^)(NSInteger))success failure:(void (^)(NSInteger))failure;

- (void)getProjectsWithSuccess:(void (^)(NSArray *))success failure:(void (^)(NSInteger))failure;

- (void)refreshBuffer:(NSString *)user success:(void (^)(NSInteger))success failure:(void (^)(NSInteger))failure;

- (void)getTSForDate:(NSDate *)date andProjectKey:(NSString *)projectKey success:(void (^)(NSArray *))success failure:(void (^)(NSInteger))failure;

- (void)reloadTSForDate:(NSDate *)date andProjectKey:(NSString *)projectKey success:(void (^)(NSArray *))success failure:(void (^)(NSInteger))failure;

- (void)approveTS:(NSDate *)date projectKey:(NSString *)projectKey TS:(NSArray *)tS success:(void (^)(NSArray *))success failure:(void (^)(NSInteger))failure;

- (void)saveTS:(NSDate *)date projectKey:(NSString *)projectKey TS:(NSArray *)tS success:(void (^)(NSArray *))success failure:(void (^)(NSInteger))failure;

- (void)deleteTS:(NSDate *)date projectKey:(NSString *)projectKey TS:(NSArray *)tS success:(void (^)(NSArray *))success failure:(void (^)(NSInteger))failure;


- (NSNumberFormatter *)defaultNumberFormatter;

@end

//
//  DataManager.m
//  ETSApproval
//
//  Created by supAdmin on 02.11.17.
//  Copyright © 2017 SAPRun. All rights reserved.
//

#import "DataManager.h"
#import "Project.h"
#import "TimeSheet.h"
#import "User.h"
#import "UIKit/UIKit.h"
#import "NSDictionary+json.h"

#import "HHFW/HHFWController.h"

#define kUrl @"http://10.144.44.144/"

// resources:
#define kResProjects 	@"ZTS_ACTIVE_PROJ_LIST1_MOBILE"
#define kResData 		@"ZTS_PM_GETDATA_RFC_MOBILE"
#define kResPreData  	@"ZTS_GET_EMP_RFC_MOBILE"
#define kSaveTSh 		@"ZTS_SAVE_PM_RFC_MOBILE"
#define kApproveTSh		@"ZTS_APPROVE_PM_RFC_MOBILE"

// keys
#define kListProjects 	@"ZTS_PROJ_LISTDESC_TT"
#define kListTSH 		@"ET_PM_PLFORMAT"

#define pUser 		@"IV_USER"
#define pDate 		@"IV_DATE"
#define pProject 	@"IV_PROJECT"
#define pProj		@"IV_PROJ"
#define pTSHApprove @"IT_PM_APP"
#define pTSHSave    @"IT_PM_CH"

#define kHTTP @"http"
#define kStatus @"status"

@interface DataManager ()

@property (nonatomic, strong) HHFWController *server;

@end

@implementation DataManager

static DataManager *sharedObject = nil;

+ (DataManager *)instance {
	static dispatch_once_t onceT = 0;
	
	dispatch_once(&onceT, ^{
		if (sharedObject == nil) {
			sharedObject = [[DataManager alloc] init];
			HHFWController *hhServer = [HHFWController sharedInstance];
			
			[hhServer initWithCredentials:@"v0.6" host:kUrl environment:@"AM" project:@"ETSApproval" application:@"app" device:[[[UIDevice currentDevice] identifierForVendor] UUIDString]];
			sharedObject.server = hhServer;
		}
	});
	return sharedObject;
}


- (void)authWithLogin:(NSString *)login password:(NSString *)password success:(void (^)(NSInteger))success failure:(void (^)(NSInteger))failure {
	
	if (isDEBUG) {
		success(NoError);
		return;
	}
	
	[[self server] auth:login password:password handler:^(id  _Nullable jsonResult) {
		NSDictionary *result = [NSDictionary jsonDictionaryWithString:jsonResult];
		int errorCode = [result[kHTTP][kStatus] intValue];
		if (errorCode == NoError) {
			[[User instance] login:login password:password token:@"token"];
			success(errorCode);
		} else {
			failure(errorCode);
		}
	}];
}

- (void)getProjectsWithSuccess:(void (^)(NSArray *))success failure:(void (^)(NSInteger))failure {
//	static int counter;
	
	if (isDEBUG) {
//		counter++;
//
//		if (counter < 3) {
//			failure(0);
//		} else {
			success(@[[[Project alloc] initWithKey:@"TEST000" name:@"Test project"]]);
//		}
		return;
	}

	TableCallParams *params = [self defaultTableParams];
	params.data = [@{pUser : [User instance].login} jsonRepresentation];
	
	[self.server table:kResProjects tableCallParams:params handler:^(id  _Nullable jsonResult) {
		NSDictionary *result = [NSDictionary jsonDictionaryWithString:jsonResult];
		int errorCode = [result[kHTTP][kStatus] intValue];
		if (errorCode != NoError) {
			failure(errorCode);
		} else {
			NSArray *projectList = result[@"result"][@"raw"];
			NSMutableArray *projects = [[NSMutableArray alloc] init];
			for (NSArray *projectArray in projectList) {
				Project *project = [[Project alloc] initWithArrayRepresentation:projectArray];
				[projects addObject:project];
			}
			success(projects);
		}
	}];        
}

- (void)getTSForDate:(NSDate *)date andProjectKey:(NSString *)projectKey success:(void (^)(NSArray *))success failure:(void (^)(NSInteger))failure {
	
	if (isDEBUG) {
		success(@[[[TimeSheet alloc] initWithJSON:@{
													@"ZPRSTGETXT": @"этап 1 тест",
													@"EMPLOYEE": @"1",
													@"EMPLOYEETXT": @"user 1",
													@"ZWTYPETXT": @"Test user 1",
													@"ZTSTXT1": @"title 1 test 123123123123",
													@"ZDAY<1>": @"3,0",
													
													}],
				  [[TimeSheet alloc] initWithJSON:@{
													@"ZPRSTGETXT": @"этап 2 тест",
													@"EMPLOYEE": @"1",
													@"EMPLOYEETXT": @"user 1",
													@"ZWTYPETXT": @"Test user 1",
													@"ZTSTXT1": @"title 2",
													@"ZDAY<3>": @"4,0",
													
													}],
				  [[TimeSheet alloc] initWithJSON:@{
													@"ZPRSTGETXT": @"этап 123 тест",
													@"EMPLOYEE": @"322",
													@"EMPLOYEETXT": @"юзверь 2",
													@"ZWTYPETXT": @"2 Test user 2",
													@"ZTSTXT1": @"random test string",
													@"ZDAY<5>": @"6,0",
													
													}]]);
		return;
	}

	
	TableCallParams *params = [self defaultTableParams];
	params.data = [@{
					   pProject:projectKey,
					   pDate:[[DataManager backendDateFormatter] stringFromDate:date],
					   } jsonRepresentation];
	
	[self.server table:kResData tableCallParams:params handler:^(id  _Nullable jsonResult) {
		NSDictionary *result = [NSDictionary jsonDictionaryWithString:jsonResult];
		int errorCode = [result[kHTTP][kStatus] intValue];
		if (errorCode != NoError) {
			failure(errorCode);
		} else {
			NSArray *timeSheetList = result[@"result"][@"raw"];;
			NSMutableArray *timeSheets = [[NSMutableArray alloc] init];
			for (NSArray *timeSheetArray in timeSheetList) {
				TimeSheet *timeSheet = [[TimeSheet alloc] initWithArrayRepresentation:timeSheetArray];
				if (![timeSheet.typeKey isEqualToString:@"SUMMARY"]) {
					[timeSheets addObject:timeSheet];
				}
			}
			return success(timeSheets);
		}
	}];
}

- (void)reloadTSForDate:(NSDate *)date andProjectKey:(NSString *)projectKey success:(void (^)(NSArray *))success failure:(void (^)(NSInteger))failure {
	
	if (isDEBUG) {
		success(@[[[TimeSheet alloc] initWithJSON:@{}]]);
		return;
	}
	
	TableCallParams *paramsEMP = [self defaultTableParams];
	paramsEMP.data = [@{
					pDate:[[DataManager backendDateFormatter] stringFromDate:date],
					pProj:projectKey,
					} jsonRepresentation];
	
	TableCallParams *params = [self defaultTableParams];
//	params.data = [@{
//					   pDate:[[DataManager backendDateFormatter] stringFromDate:date],
//					   pProject:projectKey,
//					   } jsonRepresentation];
	params.args = @{
					pDate:[[DataManager backendDateFormatter] stringFromDate:date],
					pProject:projectKey,
					};
	
	[self.server table:kResPreData tableCallParams:paramsEMP handler:^(id  _Nullable jsonResult) {
		NSDictionary *result = [NSDictionary jsonDictionaryWithString:jsonResult];
		int errorCode = [result[kHTTP][kStatus] intValue];
		if (errorCode != NoError) {
			failure(errorCode);
		} else {
			[self.server table:kResData tableCallParams:params handler:^(id  _Nullable jsonResult) {
				NSDictionary *result = [NSDictionary jsonDictionaryWithString:jsonResult];
				int errorCode = [result[kHTTP][kStatus] intValue];
				if (errorCode != NoError) {
					failure(errorCode);
				} else {
					NSArray *timeSheetList = result[@"result"][@"raw"];
					NSMutableArray *timeSheets = [[NSMutableArray alloc] init];
					for (NSArray *timeSheetArray in timeSheetList) {
						TimeSheet *timeSheet = [[TimeSheet alloc] initWithArrayRepresentation:timeSheetArray];
						if (![timeSheet.typeKey isEqualToString:@"SUMMARY"]) {
							[timeSheets addObject:timeSheet];
						}
					}
					return success(timeSheets);
				}
			}];
		}
	}];
}

- (void)saveTS:(NSDate *)date projectKey:(NSString *)projectKey TS:(NSArray *)tS success:(void (^)(NSArray *))success failure:(void (^)(NSInteger))failure {
	
	if (isDEBUG) {
		failure(0);
		//success(@[]);
		return;
	}
	
	RequestCallParams *params = [self defaultRequestParams];
	params.data = [@{
					pDate:[[DataManager backendDateFormatter] stringFromDate:date],
					pProject:projectKey,
					pTSHSave:tS, //[self arrayOfArraysToString:tS],
					} jsonRepresentation];
	[self.server request:kSaveTSh requestCallParams:params handler:^(id  _Nullable jsonResult) {
		NSDictionary *result = [NSDictionary jsonDictionaryWithString:jsonResult];
		int errorCode = [result[kHTTP][kStatus] intValue];
		if (errorCode != NoError) {
			failure(errorCode);
		} else {
			NSArray *responseMessages = result[@"result"][@"raw"];
			return success(responseMessages);
		}
	}];
}

- (void)approveTS:(NSDate *)date projectKey:(NSString *)projectKey TS:(NSArray *)tS success:(void (^)(NSArray *))success failure:(void (^)(NSInteger))failure {
	
	if (isDEBUG) {
		failure(0);
		//success(@[]);
		return;
	}
	
	RequestCallParams *params = [self defaultRequestParams];
	params.data = [@{
					pDate:[[DataManager backendDateFormatter] stringFromDate:date],
					pProject:projectKey,
					pTSHApprove:tS, //[self arrayOfArraysToString:tS],
					} jsonRepresentation];
	[self.server request:kApproveTSh requestCallParams:params handler:^(id  _Nullable jsonResult) {
		NSDictionary *result = [NSDictionary jsonDictionaryWithString:jsonResult];
		int errorCode = [result[kHTTP][kStatus] intValue];
		if (errorCode != NoError) {
			failure(errorCode);
		} else {
			NSArray *responseMessages = result[@"result"][@"raw"];
			return success(responseMessages);
		}
	}];
}

- (void)deleteTS:(NSDate *)date projectKey:(NSString *)projectKey TS:(NSArray *)tS success:(void (^)(NSArray *))success failure:(void (^)(NSInteger))failure {
	success(@[]);
}

- (void)refreshBuffer:(NSString *)user success:(void (^)(NSInteger))success failure:(void (^)(NSInteger))failure {
	RequestCallParams *params = [self defaultRequestParams];
	params.data = [@{
					pUser:user,
					} jsonRepresentation];
	[self.server request:kSaveTSh requestCallParams:params handler:^(id  _Nullable jsonResult) {
		NSDictionary *result = [NSDictionary jsonDictionaryWithString:jsonResult];
		int errorCode = [result[kHTTP][kStatus] intValue];
		if (errorCode != NoError) {
			failure(errorCode);
		} else {
			NSArray *responseMessages = result[@"result"][@"raw"];
			
			// мусорный лог, но пока-что неизвестно что делать с этими сообщениями
			NSLog(@"%@",responseMessages);
			return success(NoError);
		}
	}];
}

+ (NSDateFormatter *)backendDateFormatter {
	static NSDateFormatter *formatter;
	if (!formatter) {
		formatter = [[NSDateFormatter alloc] init];
		[formatter setDateFormat:@"YYYYMMdd"];
	}
	return formatter;
}


- (NSString *)arrayOfArraysToString:(NSArray *)array {
	NSString *result = @"";
	NSMutableArray *stringRepresentations = [[NSMutableArray alloc] init];
	for (NSArray *arrayObject in array) {
		NSString *stringRepresentation = [NSString stringWithFormat:@"\n(%@\n)", [arrayObject componentsJoinedByString:@",\n"]];
		[stringRepresentations addObject:stringRepresentation];
	}
	
	result = [NSString stringWithFormat:@"\n(%@\n)", [stringRepresentations componentsJoinedByString:@",\n"]];
	
	return result;
}

- (TableCallParams *)defaultTableParams {
	TableCallParams *params = [[TableCallParams alloc] init];
	[params initWithDefaultProperty];
	params.retryIntervalSec = 3;
	params.retryCount = 3;
	return params;
}

- (RequestCallParams *)defaultRequestParams {
	RequestCallParams *params = [[RequestCallParams alloc] init];
	[params initWithDefaultProperty];
	params.retryIntervalSec = 3;
	params.retryCount = 3;
	return params;
}

- (NSNumberFormatter *)defaultNumberFormatter {
	static NSNumberFormatter *formatter;
	if (!formatter) {
		formatter = [[NSNumberFormatter alloc] init];
		formatter.maximumFractionDigits = 2;
		formatter.minimumFractionDigits = 1;
		formatter.minimumIntegerDigits = 1;
	}
	return formatter;
}

@end

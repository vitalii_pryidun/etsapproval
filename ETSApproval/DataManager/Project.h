//
//  Project.h
//  ETSApproval
//
//  Created by supAdmin on 02.11.17.
//  Copyright © 2017 SAPRun. All rights reserved.
//

#import "BaseObject.h"

@interface Project : BaseObject

@property (strong, nonatomic) NSString *key;
@property (strong, nonatomic) NSString *name;

@property (weak, nonatomic, readonly) NSString *description;

- (instancetype)initWithKey:(NSString *)key name:(NSString *)name;

@end

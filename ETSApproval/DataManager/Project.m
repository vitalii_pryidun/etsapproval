//
//  Project.m
//  ETSApproval
//
//  Created by supAdmin on 02.11.17.
//  Copyright © 2017 SAPRun. All rights reserved.
//

#import "Project.h"

@implementation Project

- (NSString *)description {
	return [NSString stringWithFormat:@"%@ (%@)", self.key, self.name];
}

- (instancetype)initWithKey:(NSString *)key name:(NSString *)name {
	if (self = [super init]) {
		self.key = EmptyIfNil(key);
		self.name = EmptyIfNil(name);
	}
	return self;
}

- (instancetype)initWithJSON:(NSDictionary *)json {
	if (self = [super init]) {
		self.key = EmptyIfNil([json valueForKey:@"PROJECT"]);
		self.name = EmptyIfNil([json valueForKey:@"TXTSH"]);
	}
	return self;
}

- (instancetype)initWithArrayRepresentation:(NSArray *)arrayData {
	if (self = [super init]) {
		if (arrayData && arrayData.count == 2) {
			self.key = [self stringFrom:arrayData[0]];
			self.name = [self stringFrom:arrayData[1]];
		}
	}
	return self;
}

@end

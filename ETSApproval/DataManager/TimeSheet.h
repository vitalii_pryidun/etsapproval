//
//  TimeSheet.h
//  ETSApproval
//
//  Created by supAdmin on 02.11.17.
//  Copyright © 2017 SAPRun. All rights reserved.
//

#import "BaseObject.h"

@interface TimeSheet : BaseObject

@property (strong, nonatomic) NSString *stageKey;
@property (strong, nonatomic) NSString *stage;
@property (strong, nonatomic) NSString *typeKey;
@property (strong, nonatomic) NSString *type;
@property (strong, nonatomic) NSString *employeeKey;
@property (strong, nonatomic) NSString *employee;
@property (strong, nonatomic) NSString *title1;
@property (strong, nonatomic) NSString *title2;
@property (strong, nonatomic) NSString *row;
@property (strong, nonatomic) NSMutableArray *days;
@property (strong, nonatomic) NSString *sumPM;
@property (strong, nonatomic) NSString *sumFin;
@property (strong, nonatomic) NSString *realSum;

- (NSString *)title;

@end

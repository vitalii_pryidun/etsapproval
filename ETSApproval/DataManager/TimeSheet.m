//
//  TimeSheet.m
//  ETSApproval
//
//  Created by supAdmin on 02.11.17.
//  Copyright © 2017 SAPRun. All rights reserved.
//

#import "TimeSheet.h"

@implementation TimeSheet

- (instancetype)initWithJSON:(NSDictionary *)json {
	if (self = [super init]) {
		self.stageKey = 	EmptyIfNil(json[@"ZPRSTGE"]);
		self.stage = 		EmptyIfNil(json[@"ZPRSTGETXT"]);
		self.typeKey = 		EmptyIfNil(json[@"ZWTYPE"]);
		self.type = 		EmptyIfNil(json[@"ZWTYPETXT"]);
		self.employeeKey = 	EmptyIfNil(json[@"EMPLOYEE"]);
		self.employee = 	EmptyIfNil(json[@"EMPLOYEETXT"]);
		self.title1 = 		EmptyIfNil(json[@"ZTSTXT1"]);
		self.title2 = 		EmptyIfNil(json[@"ZTSTXT2"]);
		self.row = 			EmptyIfNil(json[@"ROW"]);
		self.sumPM = 		EmptyIfNil(json[@"ZSUMPM"]);
		self.sumFin = 		EmptyIfNil(json[@"ZSUMFIN"]);
		self.days = 		[@[[[self nf] numberFromString:ZeroIfNil(json[@"ZDAY<1>"])],
							   [[self nf] numberFromString:ZeroIfNil(json[@"ZDAY<2>"])],
							   [[self nf] numberFromString:ZeroIfNil(json[@"ZDAY<3>"])],
							   [[self nf] numberFromString:ZeroIfNil(json[@"ZDAY<4>"])],
							   [[self nf] numberFromString:ZeroIfNil(json[@"ZDAY<5>"])],
							   [[self nf] numberFromString:ZeroIfNil(json[@"ZDAY<6>"])],
						 	   [[self nf] numberFromString:ZeroIfNil(json[@"ZDAY<7>"])]] mutableCopy];
	}
	return self;
}

- (NSDictionary *)JSONRepresentation {
	
	NSDictionary *json = @{
						   @"ZPRSTGE" :		EmptyIfNil(self.stageKey),
						   @"ZPRSTGETXT" : 	EmptyIfNil(self.stage),
						   @"ZWTYPE" : 		EmptyIfNil(self.typeKey),
						   @"ZWTYPETXT" : 	EmptyIfNil(self.type),
						   @"EMPLOYEE" : 	EmptyIfNil(self.employeeKey),
						   @"EMPLOYEETXT" : EmptyIfNil(self.employee),
						   @"ZTSTXT1" :		EmptyIfNil(self.title1),
						   @"ZTSTXT2" : 	EmptyIfNil(self.title2),
						   @"ROW" : 		EmptyIfNil(self.row),
						   @"ZSUMPM" : 		EmptyIfNil(self.sumPM),
						   @"ZSUMFIN" : 	EmptyIfNil(self.sumFin),
						   @"ZDAY<1>" : 	ZeroIfNil([[self nf] stringFromNumber:self.days[0]]),
						   @"ZDAY<2>" : 	ZeroIfNil([[self nf] stringFromNumber:self.days[1]]),
						   @"ZDAY<3>" : 	ZeroIfNil([[self nf] stringFromNumber:self.days[2]]),
						   @"ZDAY<4>" : 	ZeroIfNil([[self nf] stringFromNumber:self.days[3]]),
						   @"ZDAY<5>" : 	ZeroIfNil([[self nf] stringFromNumber:self.days[4]]),
						   @"ZDAY<6>" : 	ZeroIfNil([[self nf] stringFromNumber:self.days[5]]),
						   @"ZDAY<7>" : 	ZeroIfNil([[self nf] stringFromNumber:self.days[6]]),
						   };
	return json;
}

- (instancetype)initWithArrayRepresentation:(NSArray *)arrayData {
	if (self = [super init]) {
		if (arrayData.count >= 18) {
			self.stageKey = 	EmptyIfNil(arrayData[0]);
			self.stage = 		EmptyIfNil(arrayData[6]);
			self.typeKey = 		EmptyIfNil(arrayData[2]);
			self.type = 		EmptyIfNil(arrayData[8]);
			self.employeeKey = 	EmptyIfNil(arrayData[1]);
			self.employee = 	[self stringFrom:(arrayData[7])];
			self.title1 = 		[self stringFrom:(arrayData[3])];
			self.title2 = 		[self stringFrom:(arrayData[4])];
			self.row = 			EmptyIfNil(arrayData[5]);
			self.days = 		[@[[[self nf] numberFromString:ZeroIfNil(arrayData[9])],
								   [[self nf] numberFromString:ZeroIfNil(arrayData[10])],
								   [[self nf] numberFromString:ZeroIfNil(arrayData[11])],
								   [[self nf] numberFromString:ZeroIfNil(arrayData[12])],
								   [[self nf] numberFromString:ZeroIfNil(arrayData[13])],
								   [[self nf] numberFromString:ZeroIfNil(arrayData[14])],
								   [[self nf] numberFromString:ZeroIfNil(arrayData[15])]] mutableCopy];
			self.sumPM = 		EmptyIfNil(arrayData[16]);
			self.sumFin = 		EmptyIfNil(arrayData[17]);
		}
	}
	return self;
}

- (NSArray *)arrayRepresentation {
	return @[
			 EmptyIfNil(self.stageKey),
			 EmptyIfNil(self.employeeKey),
			 EmptyIfNil(self.typeKey),
			 EmptyIfNil(self.title1),
			 EmptyIfNil(self.title2),
			 EmptyIfNil(self.row),
			 EmptyIfNil(self.stage),
			 EmptyIfNil(self.employee),
			 EmptyIfNil(self.type),
			 EmptyIfNil([[self nf] stringFromNumber:self.days[0]]),
			 EmptyIfNil([[self nf] stringFromNumber:self.days[1]]),
			 EmptyIfNil([[self nf] stringFromNumber:self.days[2]]),
			 EmptyIfNil([[self nf] stringFromNumber:self.days[3]]),
			 EmptyIfNil([[self nf] stringFromNumber:self.days[4]]),
			 EmptyIfNil([[self nf] stringFromNumber:self.days[5]]),
			 EmptyIfNil([[self nf] stringFromNumber:self.days[6]]),
			 EmptyIfNil(self.sumPM),
			 EmptyIfNil(self.sumFin),];
}

- (NSString *)title {
	NSString *title = EmptyIfNil(self.stage);
	if (self.title1 && ![self.title1 isEqualToString:@""]) {
		title = [title stringByAppendingString:[NSString stringWithFormat: @"\n%@", self.title1]];
	}
	if (self.title2 && ![self.title2 isEqualToString:@""]) {
		title = [title stringByAppendingString:[NSString stringWithFormat: @"\n%@", self.title2]];
	}
	return title;
}

- (NSNumberFormatter *)nf {
	static NSNumberFormatter *nf;
	if (!nf) {
		nf = [[NSNumberFormatter alloc] init];
		nf.decimalSeparator = @","; // serverside, not localized
		nf.maximumFractionDigits = 2;
		nf.minimumFractionDigits = 1;
		nf.minimumIntegerDigits = 1;
	}
	return nf;
}

- (NSString *)realSum {
	Float32 sum = 0.0;
	for (NSNumber *dayValue in self.days) {
		sum += [dayValue floatValue];
	}
	return [[self nf] stringFromNumber:@(sum)];
}

@end

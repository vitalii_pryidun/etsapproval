//
//  User.h
//  ETSApproval
//
//  Created by supAdmin on 02.11.17.
//  Copyright © 2017 SAPRun. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface User : NSObject

@property (nonatomic, strong, readonly) NSString *login;
@property (nonatomic, strong, readonly) NSString *password;
@property (nonatomic, strong, readonly) NSString *token;

+ (User *)instance;
- (void)login:(NSString *)login password:(NSString *)password token:(NSString *)token;
- (void)logout;
- (BOOL)canLogin;

@end

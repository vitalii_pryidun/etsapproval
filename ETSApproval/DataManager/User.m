//
//  User.m
//  ETSApproval
//
//  Created by supAdmin on 02.11.17.
//  Copyright © 2017 SAPRun. All rights reserved.
//

#import "User.h"
#import "Lockbox.h"
#import "Constants.h"
#import "HHFW/HHFWController.h"

@interface User()

@property (nonatomic, strong, readwrite) NSString *login;
@property (nonatomic, strong, readwrite) NSString *password;
@property (nonatomic, strong, readwrite) NSString *token;

@end


@implementation User

static User *sharedUser = nil;

+ (User *)instance {
	static dispatch_once_t onceT = 0;
	dispatch_once(&onceT, ^{
		if (sharedUser == nil) {
			sharedUser = [[User alloc] init];
			NSInteger logged = [[NSUserDefaults standardUserDefaults] integerForKey:kLoggedOnce];
			if (logged == 1) {
				sharedUser.login = [Lockbox unarchiveObjectForKey:kLoginKey];
				sharedUser.password = [Lockbox unarchiveObjectForKey:kPasswordKey];
			}
		}
	});
	return sharedUser;
}

- (void)login:(NSString *)login password:(NSString *)password token:(NSString *)token {
	self.login = login;
	self.password = password;
	self.token = token;
	
	[Lockbox archiveObject:self.token forKey:kToken];
	[Lockbox archiveObject:self.login forKey:kLoginKey];
	[Lockbox archiveObject:self.password forKey:kPasswordKey];
	
	[[NSUserDefaults standardUserDefaults] setBool:YES forKey:kLoggedOnce];
	[[NSUserDefaults standardUserDefaults] synchronize];
}

- (void)logout {
	[[HHFWController sharedInstance] unAuth];
	
	[Lockbox archiveObject:nil forKey:kToken];
	[Lockbox archiveObject:nil forKey:kLoginKey];
	[Lockbox archiveObject:nil forKey:kPasswordKey];
	
	[[NSUserDefaults standardUserDefaults] setBool:NO forKey:kLoggedOnce];
	[[NSUserDefaults standardUserDefaults] synchronize];
	
	self.login = nil;
	self.password = nil;
	self.token = nil;
}

- (BOOL)canLogin {
	BOOL logged = [[NSUserDefaults standardUserDefaults] boolForKey:kLoggedOnce];
	if (logged && self.login && ![self.login isEqualToString:@""] && self.password && ![self.password isEqualToString:@""]) {
		return YES;
	}
	return NO;
}

@end

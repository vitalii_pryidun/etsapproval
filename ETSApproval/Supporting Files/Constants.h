//
//  Constants.h
//  ETSApproval
//
//  Created by supAdmin on 02.11.17.
//  Copyright © 2017 SAPRun. All rights reserved.
//

#import <Foundation/Foundation.h>

extern NSString * const kServerName;
extern NSString * const kLoginKey;
extern NSString * const kPasswordKey;
extern NSString * const kLoggedOnce;
extern NSString * const kToken;



//
//  Constants.m
//  ETSApproval
//
//  Created by supAdmin on 02.11.17.
//  Copyright © 2017 SAPRun. All rights reserved.
//

#import "Constants.h"

NSString * const kServerName	= @"kServerName";
NSString * const kLoginKey 		= @"kLoginKey";
NSString * const kPasswordKey	= @"kPasswordKey";
NSString * const kLoggedOnce	= @"kLoggedOnce";
NSString * const kToken 		= @"kToken";

//
//  NSDate+WeekString.h
//  ETSApproval
//
//  Created by supAdmin on 03.11.17.
//  Copyright © 2017 SAPRun. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDate (WeekString)

- (NSString *)stringRepresentationOfWeek;
- (NSArray<NSString *> *)daysOfWeek;
+ (NSDate *)firstCalendarViewDay;
+ (NSDate *)lastCalendarViewDay;

@end

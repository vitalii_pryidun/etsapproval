//
//  NSDate+WeekString.m
//  ETSApproval
//
//  Created by supAdmin on 03.11.17.
//  Copyright © 2017 SAPRun. All rights reserved.
//

#import "NSDate+WeekString.h"

@implementation NSDate (WeekString)

+ (NSDate *)firstCalendarViewDay {
	
	NSString *date = [NSString stringWithFormat:@"01 01 2014"];
	return [[NSDate fullNumericDateFormatter] dateFromString:date];
}

+ (NSDate *)lastCalendarViewDay {
	NSDate *currentPlusYear = [[NSDate defaultCalendar] dateByAddingUnit:NSCalendarUnitYear value:1 toDate:[NSDate date] options:0];
	
	return currentPlusYear;
}

- (NSString *)stringRepresentationOfWeek {
	NSDate *startOfTheWeek;
	NSDate *endOfWeek;
	NSTimeInterval interval;
	[[NSDate defaultCalendar] rangeOfUnit:NSCalendarUnitWeekOfYear
				startDate:&startOfTheWeek
				 interval:&interval
				  forDate:self];
	
	endOfWeek = [startOfTheWeek dateByAddingTimeInterval:interval - 1];
	
	[[NSDate defaultCalendar] rangeOfUnit:NSCalendarUnitDay
				startDate:&endOfWeek
				 interval:NULL
				  forDate:endOfWeek];
	
	NSString *start = @"";
	if ([[NSDate defaultCalendar] compareDate:startOfTheWeek toDate:endOfWeek toUnitGranularity:NSCalendarUnitYear] == NSOrderedSame) {
		if ([[NSDate defaultCalendar] compareDate:startOfTheWeek toDate:endOfWeek toUnitGranularity:NSCalendarUnitMonth] == NSOrderedSame) {
			start = [[NSDate dayDateFormatter] stringFromDate:startOfTheWeek];
		} else {
			start = [[NSDate monthDateFormatter] stringFromDate:startOfTheWeek];
		}
	} else {
		start = [[NSDate fullDateFormatter] stringFromDate:startOfTheWeek];
	}
	NSString *end = [[NSDate fullDateFormatter] stringFromDate:endOfWeek];
	return [NSString stringWithFormat:@"%@ - %@", start, end];
}

- (NSArray<NSString *> *)daysOfWeek {
	NSMutableArray<NSString *> *days = [[NSMutableArray alloc] init];
	NSDate *day;
	NSTimeInterval interval;
	[[NSDate defaultCalendar] rangeOfUnit:NSCalendarUnitWeekOfYear
				startDate:&day
				 interval:&interval
				  forDate:self];
	
	for (NSInteger i = 0; i < 7; i++) {
		NSString *dayString = [[NSDate dayDateFormatter] stringFromDate:day];
		[days addObject:dayString];
		day = [[NSDate defaultCalendar] dateByAddingUnit:NSCalendarUnitDay value:1 toDate:day options:0];
	}
	
	return days;
}

+ (NSDateFormatter *)fullDateFormatter {
	static NSDateFormatter *formatter;
	if (formatter == nil) {
		formatter = [[NSDateFormatter alloc] init];
		[formatter setDateFormat:@"dd MMM yyyy"];
	}
	return formatter;
}

+ (NSDateFormatter *)monthDateFormatter {
	static NSDateFormatter *formatter;
	if (formatter == nil) {
		formatter = [[NSDateFormatter alloc] init];
		[formatter setDateFormat:@"dd MMM"];
	}
	return formatter;
}

+ (NSDateFormatter *)dayDateFormatter {
	static NSDateFormatter *formatter;
	if (formatter == nil) {
		formatter = [[NSDateFormatter alloc] init];
		[formatter setDateFormat:@"dd"];
	}
	return formatter;
}

+ (NSDateFormatter *)fullNumericDateFormatter {
	static NSDateFormatter *formatter;
	if (formatter == nil) {
		formatter = [[NSDateFormatter alloc] init];
		[formatter setDateFormat:@"dd mm yyyy"];
	}
	return formatter;
}

+ (NSCalendar *)defaultCalendar {
	static NSCalendar *calendar;
	if (!calendar) {
		calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
		calendar.firstWeekday = 2;
	}
	return calendar;
}

@end

//
//  NSDictionary+json.h
//  ETSApproval
//
//  Created by supAdmin on 31.01.18.
//  Copyright © 2018 SAPRun. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDictionary (json)

- (NSString *)jsonRepresentation;
+ (NSDictionary *)jsonDictionaryWithString:(NSString *)json;

@end

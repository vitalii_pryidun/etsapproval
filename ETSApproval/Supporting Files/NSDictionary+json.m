//
//  NSDictionary+json.m
//  ETSApproval
//
//  Created by supAdmin on 31.01.18.
//  Copyright © 2018 SAPRun. All rights reserved.
//

#import "NSDictionary+json.h"

@implementation NSDictionary(json)

- (NSString *)jsonRepresentation {
	NSError *error;
	NSData *jsonData = [NSJSONSerialization dataWithJSONObject:self
													   options:0
														 error:&error];
	
	if (! jsonData) {
		NSLog(@"json creating error: %@", error.localizedDescription);
		return @"{}";
	} else {
		return [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
	}
}

+ (NSDictionary *)jsonDictionaryWithString:(NSString *)json {
	NSData *webData = [json dataUsingEncoding:NSUTF8StringEncoding];
	NSError *error;
	NSDictionary *jsonDict = [NSJSONSerialization JSONObjectWithData:webData options:0 error:&error];
	return jsonDict;
}

@end

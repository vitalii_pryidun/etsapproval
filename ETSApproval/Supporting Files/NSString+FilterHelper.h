//
//  NSString+FilterHelper.h
//  ETSApproval
//
//  Created by supAdmin on 08.11.17.
//  Copyright © 2017 SAPRun. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (FilterHelper)

- (NSString *)stringByDeletingUselessText;

@end

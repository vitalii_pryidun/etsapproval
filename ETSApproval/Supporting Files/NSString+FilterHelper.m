//
//  NSString+FilterHelper.m
//  ETSApproval
//
//  Created by supAdmin on 08.11.17.
//  Copyright © 2017 SAPRun. All rights reserved.
//

#import "NSString+FilterHelper.h"

@implementation NSString (FilterHelper)

- (NSString *)stringByDeletingUselessText {
	NSString *result = self;
	NSArray *uselessChars = @[@" ", @"(", @")", @",", @"."];
	for (int i = 0; i < uselessChars.count; i++) {
		result = [result stringByReplacingOccurrencesOfString:uselessChars[i] withString:@""];
	}
	return result;
}

@end

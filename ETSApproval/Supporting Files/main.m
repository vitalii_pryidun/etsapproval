//
//  main.m
//  ETSApproval
//
//  Created by supAdmin on 01.11.17.
//  Copyright © 2017 SAPRun. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
	@autoreleasepool {
	    return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
	}
}

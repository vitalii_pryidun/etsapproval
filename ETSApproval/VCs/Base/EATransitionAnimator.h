//
//  EATransitionAnimator.h
//  ETSApproval
//
//  Created by supAdmin on 19.01.18.
//  Copyright © 2018 SAPRun. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>

@interface EATransitionAnimator : NSObject<UIViewControllerAnimatedTransitioning>

- (NSTimeInterval)transitionDuration:(id<UIViewControllerContextTransitioning>)transitionContext;
- (void)animateTransition:(id<UIViewControllerContextTransitioning>)transitionContext;

@end

//
//  EATransitionAnimator.m
//  ETSApproval
//
//  Created by supAdmin on 19.01.18.
//  Copyright © 2018 SAPRun. All rights reserved.
//

#import "EAViewController.h"
#import "EATransitionAnimator.h"

@implementation EATransitionAnimator

- (NSTimeInterval)transitionDuration:(id<UIViewControllerContextTransitioning>)transitionContext {
	return 0.35;
}


- (void)animateTransition:(id<UIViewControllerContextTransitioning>)transitionContext {
	EAViewController *toController = [transitionContext viewControllerForKey:UITransitionContextToViewControllerKey];
	[[transitionContext containerView] addSubview:toController.view];
	toController.view.alpha = 0;
	[UIView animateWithDuration:0.35 delay:0 usingSpringWithDamping:1.0 initialSpringVelocity:0 options:0 animations:^{
		toController.view.alpha = 1;
//		[toController.view layoutIfNeeded];
	} completion:^(BOOL finished) {
		[transitionContext completeTransition:![transitionContext transitionWasCancelled]];
	}];
}

@end

//
//  EAViewController.h
//  ETSApproval
//
//  Created by supAdmin on 02.11.17.
//  Copyright © 2017 SAPRun. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "DataManager.h"
#import "User.h"
#import "Constants.h"
#import "NSDate+WeekString.h"

@class Project;

@protocol TSInputSelection

@required
- (void)didSelectProject:(Project *)project;
- (void)didSelectDate:(NSDate *)date;

@end

@protocol EAKeyboardInputable

@optional
- (void)keyboardDidShow:(NSNotification *)notification;
- (void)keyboardDidHide:(NSNotification *)notification;
- (BOOL)respondsToKeyboardEvents;
- (BOOL)hidesKeyboardOnTouch;

@end

@protocol EAAnimatable

@optional
- (void)prepareForAnimation;
- (void)didEndAnimating;
- (NSArray <void (^)(void)> *)appearAnimations;
- (NSArray <void (^)(void)> *)fadeOutAnimations;
- (CGFloat)animationDuration;
- (CGFloat)delayBeetwenAnimations;
- (void)fadeOutWithCompletion:(void (^)(void))completion;

@end

@interface EAViewController : UIViewController <EAKeyboardInputable, EAAnimatable>

- (void)alertMessage:(NSString *)message title:(NSString *)title cancelButton:(NSString *)cancel;
- (void)showErrorLabel:(NSString *)errorText;
- (void)hideErrorLabel;

- (UIColor *)defaultButtonColor;

@end

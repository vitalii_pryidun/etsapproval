//
//  EAViewController.m
//  ETSApproval
//
//  Created by supAdmin on 02.11.17.
//  Copyright © 2017 SAPRun. All rights reserved.
//

#import "EAViewController.h"
#import "EATransitionAnimator.h"

@interface EAViewController () <UINavigationControllerDelegate>

@property (weak, nonatomic) IBOutlet UILabel *errorLabel;
@property (assign, nonatomic) BOOL animatedIn;

@end

@implementation EAViewController

- (void)viewDidLoad {
    [super viewDidLoad];
	[self setNeedsStatusBarAppearanceUpdate];
	[self prepareForAnimationAndReloadUI];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (UIStatusBarStyle)preferredStatusBarStyle {
	return UIStatusBarStyleLightContent;
}

#pragma mark - keyboard events

- (void)viewDidAppear:(BOOL)animated {
	self.navigationController.delegate = self;
	[super viewDidAppear:animated];
	if ([self respondsToKeyboardEvents]) {
		[[NSNotificationCenter defaultCenter] addObserver:self
												 selector:@selector(keyboardDidShow:)
													 name:UIKeyboardWillShowNotification
												   object:nil];
		
		[[NSNotificationCenter defaultCenter] addObserver:self
												 selector:@selector(keyboardDidHide:)
													 name:UIKeyboardWillHideNotification
												   object:nil];
	}
	
	if (!_animatedIn) {
		[self animateOnDisplayAndDeloadUI];
	}
}

- (void)viewDidDisappear:(BOOL)animated {
	[super viewDidDisappear:animated];
	if ([self respondsToKeyboardEvents]) {
		[[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
		[[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
	}
	
}

// for subclasses
- (void)keyboardDidShow:(NSNotification *)notification {
	
}

- (void)keyboardDidHide:(NSNotification *)notification {
	
}

- (BOOL)respondsToKeyboardEvents {
	return YES;
}

- (BOOL)hidesKeyboardOnTouch {
	return YES;
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
	if ([self hidesKeyboardOnTouch]) {
		[self.view endEditing:YES];
	}
}

#pragma mark - animation + 'virtual' methods

- (void)prepareForAnimation {
	
}

- (void)didEndAnimating {
	
}

- (NSArray<void (^)(void)> *)appearAnimations {
	return [[NSArray alloc] init];
}

- (NSArray<void (^)(void)> *)fadeOutAnimations {
	return [[NSArray alloc] init];
}

- (CGFloat)animationDuration {
	return 0.7;
}

- (CGFloat)delayBeetwenAnimations {
	return 0.1;
}

- (void)prepareForAnimationAndReloadUI {
	[self prepareForAnimation];
	[self.view layoutIfNeeded];
}

- (void)animateOnDisplayAndDeloadUI {
	self.animatedIn = YES;
	NSArray<void (^)(void)> *animations = [self appearAnimations];
	if (animations.count > 0) {
		CGFloat duration = [self animationDuration];
		CGFloat offset = 0;
		for (NSInteger i = 0; i < animations.count; i++) {
			__weak __typeof(self) weakSelf = self;
			[UIView animateWithDuration:duration delay:offset options:UIViewAnimationOptionCurveEaseInOut animations:^{
				
				void (^ animation)(void) = animations[i];
				animation();
				[weakSelf.view layoutIfNeeded];
				
			} completion:^(BOOL finished) {
				if (i == animations.count - 1) {
					[weakSelf didEndAnimating];
				}
			}];
			offset += [self delayBeetwenAnimations];
		}
	}
}

- (void)fadeOutWithCompletion:(void (^)(void))completion {
	NSArray<void (^)(void)> *animations = [self fadeOutAnimations];
	if (animations.count > 0) {
		__weak __typeof(self) weakSelf = self;
		[UIView animateWithDuration:[self animationDuration] / 2 delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
			for (NSInteger i = 0; i < animations.count; i++) {
				void (^ animation)(void) = animations[i];
				animation();
				[weakSelf.view layoutIfNeeded];
			}
		} completion:^(BOOL finished) {
			completion();
		}];
	} else {
		completion();
	}
}

#pragma mark - error handling
- (void)alertMessage:(NSString *)message title:(NSString *)title cancelButton:(NSString *)cancel {
	UIAlertController *alert = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
	NSString *cancelBtn = cancel ? cancel : @"Ок";
	[alert addAction:[UIAlertAction actionWithTitle:cancelBtn style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
		[alert dismissViewControllerAnimated:YES completion:nil];
	}]];
	[self presentViewController:alert animated:YES completion:nil];
}

- (void)showErrorLabel:(NSString *)errorText {
	self.errorLabel.text = errorText;
	self.errorLabel.hidden = NO;
}

- (void)hideErrorLabel {
	self.errorLabel.hidden = YES;
}

- (id<UIViewControllerAnimatedTransitioning>)navigationController:(UINavigationController *)navigationController animationControllerForOperation:(UINavigationControllerOperation)operation fromViewController:(UIViewController *)fromVC toViewController:(UIViewController *)toVC {
	return [[EATransitionAnimator alloc] init];
}

- (UIColor *)defaultButtonColor {
	return [UIColor colorWithRed:32.0/255 green:174.0/255 blue:231.0/255 alpha:1];
}


@end

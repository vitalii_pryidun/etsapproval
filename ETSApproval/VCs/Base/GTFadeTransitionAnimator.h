//
//  GTFadeTransitionAnimator.h
//  gpn-tenders-ios
//
//  Created by Anton Ovcharenko on 10.11.17.
//  Copyright © 2017 SAPRUN. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol GTFadeTransitionDestinationController<NSObject>

- (void)handleTransitionFinish;
- (void)prepareForAnimaton;
- (NSUInteger)numerOfAnimations;
- (CGFloat)durationOfAnimationWithIndex:(NSUInteger)index;
- (CGFloat)delayOfAnimationWithIndex:(NSUInteger)index;
- (void)makeActionsOfAnimationWithIndex:(NSUInteger)index;

@end

@interface GTFadeTransitionAnimator : NSObject<UIViewControllerAnimatedTransitioning>

- (NSTimeInterval)transitionDuration:(id<UIViewControllerContextTransitioning>)transitionContext;
- (void)animateTransition:(id<UIViewControllerContextTransitioning>)transitionContext;

@end

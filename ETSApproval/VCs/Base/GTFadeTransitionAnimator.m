//
//  GTFadeTransitionAnimator.m
//  gpn-tenders-ios
//
//  Created by Anton Ovcharenko on 10.11.17.
//  Copyright © 2017 SAPRUN. All rights reserved.
//

#import "GTFadeTransitionAnimator.h"

@interface GTFadeTransitionAnimator()

@property (nonatomic) NSUInteger completedAnimations;

@end

@implementation GTFadeTransitionAnimator

- (NSTimeInterval)transitionDuration:(id<UIViewControllerContextTransitioning>)transitionContext
{
	UIViewController<GTFadeTransitionDestinationController> *toController = [transitionContext viewControllerForKey:UITransitionContextToViewControllerKey];
	
	NSTimeInterval timeInterval = 0;
	for (NSInteger i = 0; i < [toController numerOfAnimations]; i++)
	{
		CGFloat totalDuration = [toController delayOfAnimationWithIndex:i] + [toController durationOfAnimationWithIndex:i];
		if (totalDuration > timeInterval)
		{
			timeInterval = totalDuration;
		}
	}
	return timeInterval;
}

- (NSTimeInterval)baseTransitionAnimationDuration
{
	return 0.35;
}

- (void)animateTransition:(id<UIViewControllerContextTransitioning>)transitionContext
{
	__block int completedAnimations = 0;
	UIViewController<GTFadeTransitionDestinationController> *toController = [transitionContext viewControllerForKey:UITransitionContextToViewControllerKey];
	[[transitionContext containerView] addSubview:toController.view];
	toController.view.alpha = 0;
	[toController prepareForAnimaton];
	[toController.view layoutIfNeeded];
	[UIView animateWithDuration:[self baseTransitionAnimationDuration] delay:0 usingSpringWithDamping:1.0 initialSpringVelocity:0 options:0 animations:^{
		toController.view.alpha = 1;
		[toController.view layoutIfNeeded];
	} completion:^(BOOL finished) {
		[transitionContext completeTransition:![transitionContext transitionWasCancelled]];
		if (completedAnimations == [toController numerOfAnimations])
		{
			[toController handleTransitionFinish];
		}
	}];
	for (NSUInteger index = 0; index < [toController numerOfAnimations]; index++)
	{
		[UIView animateWithDuration:[toController durationOfAnimationWithIndex:index] delay:[toController delayOfAnimationWithIndex:index] usingSpringWithDamping:1 initialSpringVelocity:0 options:0 animations:^{
			[toController makeActionsOfAnimationWithIndex:index];
			[toController.view layoutIfNeeded];
		} completion:^(BOOL finished) {
			completedAnimations++;
			if (completedAnimations == [toController numerOfAnimations])
			{
				[toController handleTransitionFinish];
			}
		}];
	}
}

@end

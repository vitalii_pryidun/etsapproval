//
//  CalendarViewController.m
//  ETSApproval
//
//  Created by supAdmin on 02.11.17.
//  Copyright © 2017 SAPRun. All rights reserved.
//

#import "CalendarViewController.h"
#import "PDTSimpleCalendarViewController.h"
#import "PDTSimpleCalendarViewCell.h"
#import "PDTSimpleCalendarViewHeader.h"
#import "PDTSimpleCalendarViewWeekdayHeader.h"

@interface CalendarViewController () <PDTSimpleCalendarViewDelegate>

@property (nonatomic, strong) PDTSimpleCalendarViewController *calendar;
@property (weak, nonatomic) IBOutlet UIView *container;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *backBtnL;
@property (weak, nonatomic) IBOutlet UIButton *backBtn;
@property (weak, nonatomic) IBOutlet UIStackView *days;
@property (weak, nonatomic) IBOutlet UIView *separator;

@end

@implementation CalendarViewController

- (void)viewDidLoad {
    [super viewDidLoad];
	[self showCalendar];
}

- (void)viewDidAppear:(BOOL)animated {
	[super viewDidAppear:animated];
}


- (void)showCalendar {
	[[PDTSimpleCalendarViewHeader appearance] setTextColor:[UIColor whiteColor]];
	[[PDTSimpleCalendarViewHeader appearance] setSeparatorColor:[[UIColor whiteColor] colorWithAlphaComponent:0.5]];
	
	[[PDTSimpleCalendarViewWeekdayHeader appearance] setHeaderBackgroundColor:[UIColor clearColor]];
	[[PDTSimpleCalendarViewWeekdayHeader appearance] setTextColor:[UIColor whiteColor]];
	
	[[PDTSimpleCalendarViewCell appearance] setCircleDefaultColor:[UIColor clearColor]];
	[[PDTSimpleCalendarViewCell appearance] setCircleSelectedColor:[self defaultButtonColor]];
	[[PDTSimpleCalendarViewCell appearance] setCircleTodayColor:[[self defaultButtonColor] colorWithAlphaComponent:0.5]];
	[[PDTSimpleCalendarViewCell appearance] setTextDefaultColor:[UIColor whiteColor]];
	[[PDTSimpleCalendarViewCell appearance] setTextSelectedColor:[UIColor whiteColor]];
	[[PDTSimpleCalendarViewCell appearance] setTextTodayColor:[UIColor whiteColor]];

	PDTSimpleCalendarViewController *calendarVC = [[PDTSimpleCalendarViewController alloc] init];
	
	calendarVC.backgroundColor = [UIColor clearColor];
	calendarVC.view.backgroundColor = [UIColor clearColor];
	calendarVC.calendar.firstWeekday = 2;
	calendarVC.firstDate = [NSDate firstCalendarViewDay];
	calendarVC.lastDate = [NSDate lastCalendarViewDay];
	calendarVC.selectedDate = [NSDate date];
	[calendarVC setDelegate:self];
	if ([UIViewController instancesRespondToSelector:@selector(edgesForExtendedLayout)]) {
		[calendarVC setEdgesForExtendedLayout:UIRectEdgeNone];
	}
	
	self.calendar = calendarVC;
	[calendarVC willMoveToParentViewController:self];
	[self.container addSubview:calendarVC.view];
	[self addChildViewController:calendarVC];
	[calendarVC didMoveToParentViewController:self];
	
	[calendarVC scrollToSelectedDate:NO];
}

- (void)simpleCalendarViewController:(PDTSimpleCalendarViewController *)controller didSelectDate:(NSDate *)date {
	NSDate *realDate = [[NSCalendar calendarWithIdentifier:NSCalendarIdentifierGregorian] dateByAddingUnit:NSCalendarUnitDay value:1 toDate:date options:0];
	[self.delegate didSelectDate:realDate];
	[self.navigationController popViewControllerAnimated:YES];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillLayoutSubviews {
	[super viewWillLayoutSubviews];
	[self.calendar viewWillLayoutSubviews];
}

- (void)viewDidLayoutSubviews {
	[super viewDidLayoutSubviews];
	[self.calendar viewDidLayoutSubviews];
	[self.calendar scrollToSelectedDate:YES];
}

- (BOOL)simpleCalendarViewController:(PDTSimpleCalendarViewController *)controller shouldUseCustomColorsForDate:(NSDate *)date {
	return NO;
}

- (IBAction)backBtnTouched:(id)sender {
	[self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - animations
- (void)prepareForAnimation {
	self.backBtn.alpha = 0;
	self.backBtnL.constant += 200;
	self.container.alpha = 0;
	self.days.alpha = 0;
	self.separator.alpha = 0;
}

- (NSArray<void (^)(void)> *)appearAnimations {
	__weak __typeof(self) weakSelf = self;
	return @[^(){
				weakSelf.backBtnL.constant -= 200;
				weakSelf.backBtn.alpha = 1;
				weakSelf.days.alpha = 1;
			 },
			 ^(){
				weakSelf.container.alpha = 1;
				weakSelf.separator.alpha = 0.2;
			 }];
}

@end

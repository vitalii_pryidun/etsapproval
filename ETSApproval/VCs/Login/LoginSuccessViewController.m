//
//  LoginSuccessViewController.m
//  ETSApproval
//
//  Created by supAdmin on 19.01.18.
//  Copyright © 2018 SAPRun. All rights reserved.
//

#import "LoginSuccessViewController.h"

@interface LoginSuccessViewController ()

@property (weak, nonatomic) IBOutlet UIView *shadowView;
@property (weak, nonatomic) IBOutlet UIImageView *logoImage;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *logoCenter;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *imageCenter;
@property (weak, nonatomic) IBOutlet UILabel *textLabel;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *okImageH;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *okImageW;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *margin;
@property (weak, nonatomic) IBOutlet UIImageView *okImage;

@end

@implementation LoginSuccessViewController

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)prepareForAnimation {
	self.shadowView.alpha = 0.1;
	self.textLabel.alpha = 0;
}

- (NSArray<void (^)(void)> *)appearAnimations {
	__weak __typeof(self) weakSelf = self;
	return @[
			 	^(){weakSelf.shadowView.alpha = 0.5;},
				 
				 ^(){weakSelf.okImageH.constant = 150;
					 weakSelf.okImageW.constant = 150;
					 weakSelf.imageCenter.constant = -80;
					 weakSelf.margin.constant = 40;
				 },
				 
				 ^(){weakSelf.logoImage.alpha = 0;
					 weakSelf.logoCenter.constant = -weakSelf.view.frame.size.height / 2;
					 weakSelf.textLabel.alpha = 1;
				 },
			  
			 ];
}

- (NSArray<void (^)(void)> *)fadeOutAnimations {
	__weak __typeof(self) weakSelf = self;
	return @[
			 	^(){
					weakSelf.shadowView.alpha = 0.7;
					weakSelf.okImage.alpha = 0;
					weakSelf.textLabel.alpha = 0;
					CGFloat size = MAX(weakSelf.view.frame.size.height, weakSelf.view.frame.size.width);
					weakSelf.okImageW.constant = size;
					weakSelf.okImageH.constant = size;
				}
			];
}

- (void)pushLoadViewController {
	__weak __typeof(self) weakSelf = self;
	[self fadeOutWithCompletion:^{
		[weakSelf performSegueWithIdentifier:@"load" sender:nil];
	}];
}

- (void)didEndAnimating {
	[self performSelector:@selector(pushLoadViewController) withObject:nil afterDelay:0.5];
}

@end

//
//  LoginViewController.m
//  ETSApproval
//
//  Created by supAdmin on 02.11.17.
//  Copyright © 2017 SAPRun. All rights reserved.
//

#import "LoginViewController.h"
#import <LocalAuthentication/LocalAuthentication.h>
#import "UIView+Shake.h"
#import "TSLoadingViewController.h"

#define kLogoMarginDefault 40
#define kLogoMarginStart -114

@interface LoginViewController()<UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UITextField *loginTF;
@property (weak, nonatomic) IBOutlet UITextField *passwordTF;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;
@property (weak, nonatomic) IBOutlet UIButton *enterBtn;
@property (weak, nonatomic) IBOutlet UIButton *LALoginBtn;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *center;
@property (weak, nonatomic) IBOutlet UIView *passContainer;
@property (weak, nonatomic) IBOutlet UIView *loginContainer;
@property (weak, nonatomic) IBOutlet UIView *shadowView;

@property (strong, nonatomic) LAContext *authContext;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *logoBtnConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *enterBtnWidth;

@end

@implementation LoginViewController

- (void)viewDidLoad {
    [super viewDidLoad];
	
	UIColor *color = [UIColor whiteColor];
	NSString *placeholderKey = @"_placeholderLabel.textColor";
	
	self.loginContainer.layer.borderColor = color.CGColor;
	self.passContainer.layer.borderColor = color.CGColor;
	[self.loginTF setValue:[color colorWithAlphaComponent:0.5]
				forKeyPath:placeholderKey];
	[self.passwordTF setValue:[color colorWithAlphaComponent:0.5]
				   forKeyPath:placeholderKey];
}

- (void)viewWillAppear:(BOOL)animated {
	[super viewWillAppear:animated];
	
	BOOL canLogin = [[User instance] canLogin];
	
	self.authContext = [[LAContext alloc] init];
	
	if ([self.authContext canEvaluatePolicy:LAPolicyDeviceOwnerAuthenticationWithBiometrics error:nil] && canLogin) {
		self.LALoginBtn.hidden = NO;
		if (@available(iOS 11.0, *)) {
			if (self.authContext.biometryType == LABiometryTypeFaceID) {
				[self.LALoginBtn setImage:[UIImage imageNamed:@"FaceID"] forState:UIControlStateNormal];
			}
		}
	} else {
		self.LALoginBtn.hidden = YES;
	}
	
	if (isDEBUG) {
		self.loginTF.text = @"test";
		self.passwordTF.text = @"testtest";
	}
}

- (void)viewDidDisappear:(BOOL)animated {
	[super viewDidDisappear:animated];

	self.enterBtnWidth.constant = 0;
	[self.view layoutIfNeeded];
}

- (void)viewWillDisappear:(BOOL)animated {
	[super viewWillDisappear:animated];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (IBAction)enterBtnTouched:(id)sender {
	[self.loginTF resignFirstResponder];
	[self.passwordTF resignFirstResponder];
	NSString *login = self.loginTF.text;
	if (!login || [login isEqualToString:@""]) {
		[self.loginContainer shake];
		return;
	}
	NSString *password = self.passwordTF.text;
	if (!password || [password isEqualToString:@""]) {
		[self.passContainer shake];
		return;
	}
	
	
	[self.activityIndicator startAnimating];
	[self.enterBtn setTitle:@"" forState:UIControlStateNormal];
	[self.enterBtn setUserInteractionEnabled:NO];
	
	__weak __typeof(self) weakSelf = self;
	[UIView animateWithDuration:0.35 animations:^{
		CGFloat difference = weakSelf.passContainer.frame.size.width - 50;
		weakSelf.enterBtnWidth.constant = -difference;
		[weakSelf.view layoutIfNeeded];
	} completion:^(BOOL finished) {
		[[DataManager instance] authWithLogin:login password:password success:^(NSInteger code) {
			[weakSelf handleSuccess];
		} failure:^(NSInteger code) {
			[weakSelf handleFailure:code];
		}];
	}];
}


- (void)handleSuccess {
	[self.activityIndicator stopAnimating];
	[self performSegueWithIdentifier:@"login" sender:self];
}

- (void)handleFailure:(NSInteger)errCode {
	__weak __typeof(self) weakSelf = self;
	[UIView animateWithDuration:0.35 animations:^{
		weakSelf.enterBtnWidth.constant = 0;
		[weakSelf.view layoutIfNeeded];
	} completion:^(BOOL finished) {
		[weakSelf.enterBtn setTitle:@"ВОЙТИ" forState:UIControlStateNormal];
		[weakSelf.activityIndicator stopAnimating];
		[weakSelf.enterBtn setUserInteractionEnabled:YES];
		
		[self.enterBtn shake];
	}];
	
	if (errCode == 1) {
		[self alertMessage:@"Не удалось войти, проверьте подключение к сети" title:nil cancelButton:nil];
	} else if (errCode == 2) {
		[self alertMessage:@"Пользователь с таким логином и/или паролем не зарегистрирован в системе" title:nil cancelButton:nil];
	}
	
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
	if (textField == self.loginTF) {
		[self.passwordTF becomeFirstResponder];
	} else {
		[textField resignFirstResponder];
		[self enterBtnTouched:nil];
	}
	return YES;
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
	// Workaround for the jumping text bug.
	[textField resignFirstResponder];
	[textField layoutIfNeeded];
}

- (void)keyboardDidShow:(NSNotification *)notification {
	CGSize keyboardSize = [[[notification userInfo] objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].size;
	CGFloat contentBottom = 0;
	UIInterfaceOrientation interfaceOrientation = [[UIApplication sharedApplication] statusBarOrientation];
	if (UIInterfaceOrientationIsPortrait(interfaceOrientation)) {
		contentBottom = self.enterBtn.frame.origin.y + self.enterBtn.frame.size.height;
	} else {
		contentBottom = self.passContainer.frame.origin.y + self.passContainer.frame.size.height;
	}
	CGFloat keyboardTop = self.view.frame.size.height - keyboardSize.height;
	if (keyboardTop < contentBottom) {
		__weak typeof(self) weakSelf = self;
		[UIView animateWithDuration:0.1 animations:^{
			weakSelf.center.constant = -(contentBottom - keyboardTop) - 8;
			[weakSelf.view layoutIfNeeded];
		}];
	}
}

- (void)keyboardDidHide:(NSNotification *)notification {
	if (self.center.constant != 0) {
		__weak typeof(self) weakSelf = self;
		[UIView animateWithDuration:0.1 animations:^{
			weakSelf.center.constant = 0;
			[weakSelf.view layoutIfNeeded];
		}];
	}
}

- (IBAction)unwindToLogin:(UIStoryboardSegue *)segue {
	[[User instance] logout];
	self.loginTF.text = @"";
	self.passwordTF.text = @"";
	
	[self.enterBtn setTitle:@"ВОЙТИ" forState:UIControlStateNormal];
	[self.enterBtn setUserInteractionEnabled:YES];
}

- (IBAction)LABtnTouched:(id)sender {
	NSError *authError = nil;
	if ([self.authContext canEvaluatePolicy:LAPolicyDeviceOwnerAuthenticationWithBiometrics error:&authError]) {
		__weak __typeof(self) weakSelf = self;
		[self.authContext evaluatePolicy:LAPolicyDeviceOwnerAuthenticationWithBiometrics localizedReason:@"для входа в приложение" reply:^(BOOL success, NSError *error) {
			if (success) {
				dispatch_async(dispatch_get_main_queue(), ^{
					weakSelf.loginTF.text = [User instance].login;
					weakSelf.passwordTF.text = [User instance].password;
					[weakSelf enterBtnTouched:nil];
				});
			} else {
				dispatch_async(dispatch_get_main_queue(), ^{
					[weakSelf.LALoginBtn shake];
				});
			}
		}];
	}
}

- (void)prepareForAnimation {
	self.shadowView.alpha = 0;
	self.loginContainer.alpha = 0;
	self.passContainer.alpha = 0;
	self.enterBtn.alpha = 0;
	self.LALoginBtn.alpha = 0;
	self.view.userInteractionEnabled = NO;
	self.center.constant = -(kLogoMarginStart - kLogoMarginDefault);
}

- (NSArray<void (^)(void)> *)appearAnimations {
	__weak __typeof(self) weakSelf = self;
	return @[
			 	^(){weakSelf.shadowView.alpha = 0.1;},
			 	^(){weakSelf.center.constant = 0;},
				^(){weakSelf.loginContainer.alpha = 1;},
				^(){weakSelf.passContainer.alpha = 1;},
				^(){weakSelf.enterBtn.alpha = 1;},
				^(){weakSelf.LALoginBtn.alpha = 1;},
			 ];
}

- (CGFloat)delayBeetwenAnimations {
	return 0.3;
}

- (CGFloat)animationDuration {
	return 0.75;
}

- (void)didEndAnimating {
	self.view.userInteractionEnabled = YES;
}

@end

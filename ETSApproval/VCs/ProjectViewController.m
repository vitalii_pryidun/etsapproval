//
//  ProjectViewController.m
//  ETSApproval
//
//  Created by supAdmin on 02.11.17.
//  Copyright © 2017 SAPRun. All rights reserved.
//

#import "ProjectViewController.h"
#import "Project.h"
#import "TSLoadingViewController.h"
#import "NSString+FilterHelper.h"

@interface ProjectViewController () <UITableViewDelegate, UITableViewDataSource, UISearchResultsUpdating, UISearchBarDelegate>

// data
@property (strong, nonatomic) NSArray *projects;
@property (strong, nonatomic) NSArray *filteredProjects;

// UI
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *tableviewBottom;
@property (strong, nonatomic) IBOutlet UISearchBar *searchController;
@property (weak, nonatomic) IBOutlet UIButton *backBtn;
@property (weak, nonatomic) IBOutlet UIButton *searchBtn;
@property (weak, nonatomic) IBOutlet UIImageView *successImage;
@property (weak, nonatomic) IBOutlet UIButton *reloadBtn;

// animations
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *backBtnL;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *searchBtnT;


@end

@implementation ProjectViewController

- (void)viewDidLoad {
    [super viewDidLoad];
	self.filteredProjects = @[];
	self.projects = @[];
	[self.tableView registerClass:[UITableViewCell class] forCellReuseIdentifier:@"UITableViewCell"];
	
	[self loadProjects];
	
	self.definesPresentationContext = YES;
}

- (IBAction)reloadBtnTouched:(id)sender {
	[self loadProjects];
}

- (void)loadProjects {
	[self prepareUIForLoading];
	__weak __typeof(self) weakSelf = self;
	[[DataManager instance] getProjectsWithSuccess:^(NSArray *projects) {
		weakSelf.projects = projects;
		weakSelf.filteredProjects = projects;
		[weakSelf handleLoadingSuccess];
	} failure:^(NSInteger errCode) {
		weakSelf.projects = @[];
		weakSelf.filteredProjects = @[];
		[weakSelf handleLoadingFailure];
	}];
}

- (void)prepareUIForLoading {
	[self.activityIndicator startAnimating];
	self.tableView.hidden = YES;
	self.successImage.hidden = NO;
	self.successImage.alpha = 1;
	self.successImage.backgroundColor = [self defaultButtonColor];
	self.reloadBtn.hidden = YES;
}

- (void)handleLoadingSuccess {
	[self.activityIndicator stopAnimating];
	[self hideErrorLabel];
	self.successImage.backgroundColor = [UIColor clearColor];
	__weak __typeof(self) weakSelf = self;
	[UIView animateWithDuration:0.35 delay:0.5 options:UIViewAnimationOptionCurveEaseOut animations:^{
		weakSelf.successImage.alpha = 0;
	} completion:^(BOOL finished) {
		weakSelf.successImage.hidden = YES;
		weakSelf.tableView.hidden = NO;
		[weakSelf.tableView reloadSections:[NSIndexSet indexSetWithIndex:0] withRowAnimation:UITableViewRowAnimationFade];
	}];
}

- (void)handleLoadingFailure {
	[self.activityIndicator stopAnimating];
	[self.tableView reloadData];
	[self showErrorLabel:@"Не удалось загрузить список проектов"];
	self.successImage.hidden = YES;
	self.tableView.hidden = YES;
	self.reloadBtn.hidden = NO;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (nonnull UITableViewCell *)tableView:(nonnull UITableView *)tableView cellForRowAtIndexPath:(nonnull NSIndexPath *)indexPath {
	Project *proj = self.filteredProjects[indexPath.row];
	
	UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"UITableViewCell" forIndexPath:indexPath];
	cell.textLabel.text = proj.description;
	cell.textLabel.numberOfLines = 0;
	cell.textLabel.textColor = [[UIColor whiteColor] colorWithAlphaComponent:0.82];
	cell.backgroundColor = [UIColor clearColor];
	cell.contentView.backgroundColor = [UIColor clearColor];
	cell.selectionStyle = UITableViewCellSelectionStyleNone;
	return cell;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
	return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
	return UITableViewAutomaticDimension;
}

- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath {
	return UITableViewAutomaticDimension;
}

- (NSInteger)tableView:(nonnull UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
	return self.filteredProjects.count;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
	Project *project = self.filteredProjects[self.tableView.indexPathsForSelectedRows.firstObject.row];
	[self.delegate didSelectProject:project];
	[self.navigationController popViewControllerAnimated:YES];
}

- (void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath {
	if ([self.tableView indexPathsForSelectedRows].count == 0) {
		
	}
}

#pragma mark - keyboard events

- (void)keyboardDidShow:(NSNotification *)notification {
	CGSize keyboardSize = [[[notification userInfo] objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].size;
	__weak typeof(self) weakSelf = self;
	[UIView animateWithDuration:0.1 animations:^{
		weakSelf.tableviewBottom.constant = keyboardSize.height;
		[weakSelf.view layoutIfNeeded];
	}];
}

- (void)keyboardDidHide:(NSNotification *)notification {
	__weak typeof(self) weakSelf = self;
	[UIView animateWithDuration:0.1 animations:^{
		weakSelf.tableviewBottom.constant = 0;
		[weakSelf.view layoutIfNeeded];
	}];
}

- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar {
	self.filteredProjects = self.projects;
	[self.tableView reloadData];
}

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText {
	if ([searchBar.text isEqualToString:@""]) {
		self.filteredProjects = self.projects;
		[self.tableView reloadData];
	} else {
		self.filteredProjects = [self.projects filteredArrayUsingPredicate:[NSPredicate predicateWithBlock:^BOOL(Project *evaluatedObject, NSDictionary<NSString *,id> * _Nullable bindings) {
			NSString *text = [[evaluatedObject.description lowercaseString] stringByDeletingUselessText];
			NSString *searchedText = [[searchBar.text lowercaseString] stringByDeletingUselessText];
			
			return [text rangeOfString:searchedText].location != NSNotFound;
		}]];
		[self.tableView reloadData];
	}
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
	[searchBar resignFirstResponder];
}

- (void)didPresentSearchController:(UISearchController *)searchController {
	[searchController.searchBar becomeFirstResponder];
}

- (IBAction)searchBtnTouched:(id)sender {
	__weak __typeof(self) weakSelf = self;
	dispatch_async(dispatch_get_main_queue(), ^{
		[weakSelf.searchController becomeFirstResponder];
	});
}

- (IBAction)backBtnTouched:(id)sender {
	[self.navigationController popViewControllerAnimated:YES];
}

- (void)prepareForAnimation {
	self.backBtnL.constant += 40;
	self.searchBtnT.constant += 40;
	self.backBtn.alpha = 0;
	self.searchBtn.alpha = 0;
	self.successImage.alpha = 0;
}

- (NSArray<void (^)(void)> *)appearAnimations {
	__weak __typeof(self) weakSelf = self;
	return @[ ^(){
		weakSelf.backBtnL.constant -= 40;
		weakSelf.searchBtnT.constant -= 40;
		weakSelf.backBtn.alpha = 1;
		weakSelf.searchBtn.alpha = 1;
		weakSelf.successImage.alpha = 1;
	} ];
}

@end


//
//  ResultViewController.h
//  ETSApproval
//
//  Created by supAdmin on 29.01.18.
//  Copyright © 2018 SAPRun. All rights reserved.
//

#import "EAViewController.h"

typedef NS_ENUM(NSInteger, ResultTSAction){
	save,
	approve,
	clear,
};


@interface ResultViewController : EAViewController

@property (nonatomic, assign) ResultTSAction action;
@property (nonatomic, strong) NSDate *date;
@property (nonatomic, strong) NSString *projectKey;
@property (nonatomic, strong) NSArray *ts;

@end

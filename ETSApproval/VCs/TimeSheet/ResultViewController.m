//
//  ResultViewController.m
//  ETSApproval
//
//  Created by supAdmin on 29.01.18.
//  Copyright © 2018 SAPRun. All rights reserved.
//

#import "ResultViewController.h"

@interface ResultViewController ()

// ui-components
@property (weak, nonatomic) IBOutlet UIImageView *successImage;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;
@property (weak, nonatomic) IBOutlet UILabel *messageLabel;
@property (weak, nonatomic) IBOutlet UIButton *retryBtn;
@property (weak, nonatomic) IBOutlet UIView *shadowView;
@property (weak, nonatomic) IBOutlet UIButton *backBtn;

// animations
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *imageW;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *imageH;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *msgLabelCenter;

@end

@implementation ResultViewController

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void)viewDidAppear:(BOOL)animated {
	[self performAction];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)performAction {
	__weak __typeof(self) weakSelf = self;
	[self setLoadingStateWithCompletion:^{
		if (weakSelf.action == save) {
			[weakSelf saveTS];
		} else if (weakSelf.action == approve) {
			[weakSelf approveTS];
		} else if (weakSelf.action == clear) {
			[weakSelf deleteTS];
		}
	}];
}

- (void)saveTS {
	__weak __typeof(self) weakSelf = self;
	[[DataManager instance] saveTS:self.date projectKey:self.projectKey TS:self.ts success:^(NSArray *messages) {
		weakSelf.messageLabel.text = @"ТШ успешно сохранены!";
		[weakSelf setSuccessState];
	} failure:^(NSInteger error) {
		weakSelf.messageLabel.text = @"Не удалось сохранить данные.\nПроверьте подключение к сети";
		[weakSelf setFailureState];
	}];
}

- (void)approveTS {
	__weak __typeof(self) weakSelf = self;
	[[DataManager instance] approveTS:self.date projectKey:self.projectKey TS:self.ts success:^(NSArray *messages) {
		weakSelf.messageLabel.text = @"ТШ успешно утверждены!";
		[weakSelf setSuccessState];
	} failure:^(NSInteger error) {
		weakSelf.messageLabel.text = @"Не удалось утвердить ТШ.\nПроверьте подключение к сети";
		[weakSelf setFailureState];
	}];
}

- (void)deleteTS {
	__weak __typeof(self) weakSelf = self;
	[[DataManager instance] deleteTS:self.date projectKey:self.projectKey TS:self.ts success:^(NSArray *messages) {
		weakSelf.messageLabel.text = @"ТШ успешно удалены!";
		[weakSelf setSuccessState];
	} failure:^(NSInteger error) {
		weakSelf.messageLabel.text = @"Не удалось удалить ТШ.\nПроверьте подключение к сети";
		[weakSelf setFailureState];
	}];
}

- (void)setLoadingStateWithCompletion:(void (^)(void))completion {
	self.retryBtn.hidden = YES;
	self.backBtn.hidden = YES;
	self.successImage.backgroundColor = [self defaultButtonColor];
	[self.activityIndicator startAnimating];
	__weak __typeof(self) weakSelf = self;
	[UIView animateWithDuration:0.35 delay:0 options:UIViewAnimationOptionCurveEaseInOut animations:^{
		weakSelf.imageH.constant = 50;
		weakSelf.imageW.constant = 50;
		weakSelf.successImage.layer.cornerRadius = 25;
		[weakSelf.view layoutIfNeeded];
		weakSelf.messageLabel.alpha = 0;
	} completion:^(BOOL finished) {
		if (completion) {
			completion();
		}
	}];
}

- (void)setSuccessState {
	self.messageLabel.hidden = NO;
	self.retryBtn.hidden = YES;
	self.backBtn.hidden = YES;
	self.successImage.image = [UIImage imageNamed:@"ok"];
	self.successImage.backgroundColor = [UIColor clearColor];
	[self.activityIndicator stopAnimating];
	__weak __typeof(self) weakSelf = self;
	[UIView animateWithDuration:0.35 delay:0 options:UIViewAnimationOptionCurveEaseInOut animations:^{
		weakSelf.imageH.constant = 120;
		weakSelf.imageW.constant = 120;
		weakSelf.successImage.layer.cornerRadius = 60;
		[weakSelf.view layoutIfNeeded];
		weakSelf.messageLabel.alpha = 1;
	} completion:^(BOOL finished) {
		[weakSelf performSelector:@selector(unwindToLoadingTS) withObject:nil afterDelay:2];
	}];
}

- (void)setFailureState {
	self.messageLabel.hidden = NO;
	self.retryBtn.hidden = NO;
	self.backBtn.hidden = NO;
	self.successImage.image = [UIImage imageNamed:@"failure"];
	self.successImage.backgroundColor = [UIColor clearColor];
	[self.activityIndicator stopAnimating];
	__weak __typeof(self) weakSelf = self;
	[UIView animateWithDuration:0.35 delay:0 options:UIViewAnimationOptionCurveEaseInOut animations:^{
		weakSelf.imageH.constant = 120;
		weakSelf.imageW.constant = 120;
		weakSelf.successImage.layer.cornerRadius = 60;
		[weakSelf.view layoutIfNeeded];
		weakSelf.messageLabel.alpha = 1;
	} completion:nil];
}

- (IBAction)retryBtnTouched:(id)sender {
	[self performAction];
}

- (IBAction)backBtnTouched:(id)sender {
	[self.navigationController popViewControllerAnimated:YES];
}

- (void)unwindToLoadingTS {
	__weak __typeof(self) weakSelf = self;
	[self fadeOutWithCompletion:^{
		[weakSelf performSegueWithIdentifier:@"goBack" sender:self];
	}];
}

- (NSArray<void (^)(void)> *)fadeOutAnimations {
	__weak __typeof(self) weakSelf = self;
	return @[
			 ^(){
				 weakSelf.shadowView.alpha = 0.7;
				 weakSelf.successImage.alpha = 0;
				 weakSelf.messageLabel.alpha = 0;
				 CGFloat size = MAX(weakSelf.view.frame.size.height, weakSelf.view.frame.size.width);
				 weakSelf.imageW.constant = size;
				 weakSelf.imageH.constant = size;
				 weakSelf.msgLabelCenter.constant = size / 2;
			 }
			];
}

@end

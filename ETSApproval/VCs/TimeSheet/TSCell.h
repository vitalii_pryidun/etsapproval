//
//  TSCell.h
//  ETSApproval
//
//  Created by supAdmin on 02.11.17.
//  Copyright © 2017 SAPRun. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSInteger, CellPosition) {
	PositionNormal = 0,
	PositionLast = 1,
};

@class TimeSheet;

@interface TSCell : UITableViewCell

@property (strong, nonatomic) TimeSheet *timeSheet;
@property (assign, nonatomic) CellPosition position;
@property (weak, nonatomic) IBOutlet UILabel *timeLabel;
@property (strong, nonatomic) IBOutletCollection(UITextField) NSArray *textFields;

@end

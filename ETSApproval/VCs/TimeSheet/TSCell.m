//
//  TSCell.m
//  ETSApproval
//
//  Created by supAdmin on 02.11.17.
//  Copyright © 2017 SAPRun. All rights reserved.
//

#import "TSCell.h"
#import "TimeSheet.h"

@interface TSCell ()

@property (weak, nonatomic) IBOutlet UILabel *dataLabel;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *tfHeight;
@property (weak, nonatomic) IBOutlet UIView *innerView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *bottomConstraint;
@property (weak, nonatomic) IBOutlet UIView *shadowContainer;
@property (weak, nonatomic) IBOutlet UIView *separator;

@end

@implementation TSCell

- (void)awakeFromNib {
    [super awakeFromNib];
	
	self.shadowContainer.layer.shadowColor = [UIColor blackColor].CGColor;
	self.shadowContainer.layer.shadowRadius = 8.f;
	self.shadowContainer.layer.shadowOffset = CGSizeMake(0.f, 4.f);
	self.shadowContainer.layer.shadowOpacity = 0.1f;
	self.shadowContainer.layer.masksToBounds = YES;
	
	for (NSInteger i = 0; i < self.textFields.count; i++) {
		UITextField *tf = self.textFields[i];
		tf.inputAccessoryView = [self inputAccessoryViewForIndex:i];
	}
	[self.contentView setNeedsLayout];
	[self.contentView layoutIfNeeded];
}

- (void)setPosition:(CellPosition)position {
	_position = position;
	[self.contentView layoutIfNeeded];
	UIBezierPath *shadowPath;
	if (position == PositionNormal) {
		self.bottomConstraint.constant = 0;
		self.separator.hidden = NO;
		shadowPath = [UIBezierPath bezierPathWithRoundedRect:self.innerView.bounds byRoundingCorners:(UIRectCornerBottomLeft|UIRectCornerBottomRight|UIRectCornerTopLeft) cornerRadii:CGSizeMake(0.0, 0.0)];
	} else {
		self.bottomConstraint.constant = 20;
		self.separator.hidden = YES;
		shadowPath = [UIBezierPath bezierPathWithRoundedRect:self.innerView.bounds byRoundingCorners:(UIRectCornerBottomLeft|UIRectCornerBottomRight) cornerRadii:CGSizeMake(25.0, 25.0)];
	}
	CAShapeLayer *maskLayer = [CAShapeLayer layer];
	maskLayer.path = shadowPath.CGPath;
	maskLayer.frame = self.innerView.bounds;
	self.innerView.layer.mask = maskLayer;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

- (UIView *)inputAccessoryViewForIndex:(NSUInteger)index {
	UIToolbar * inputAccessoryView = [[UIToolbar alloc] init];
	inputAccessoryView.barStyle = UIBarStyleBlack;
	inputAccessoryView.translucent = YES;
	[inputAccessoryView sizeToFit];
	inputAccessoryView.userInteractionEnabled = YES;

	NSInteger day = index % 10;
	UIImage *back = [UIImage imageNamed:@"back"];
	UIImage *forward = [UIImage imageNamed:@"forward"];
	
	UIBarButtonItem *prevBtn = [[UIBarButtonItem alloc] initWithImage:back style:UIBarButtonItemStylePlain target:self action:@selector(prevTF:)];
	prevBtn.tag = index;
	prevBtn.enabled = day > 0;
	UIBarButtonItem *nextBtn = [[UIBarButtonItem alloc] initWithImage:forward style:UIBarButtonItemStylePlain target:self action:@selector(nextTF:)];
	nextBtn.tag = index;
	nextBtn.enabled = day < 6;
	UIBarButtonItem *doneBtn = [[UIBarButtonItem alloc] initWithTitle:@"Готово" style:UIBarButtonItemStylePlain target:self action:@selector(doneInput:)];
	UIBarButtonItem *flexible = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
	UIBarButtonItem *fixed = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
	fixed.width = 20;
	[inputAccessoryView setItems:@[fixed, prevBtn, fixed, nextBtn, flexible, doneBtn] animated:NO];
	
	return inputAccessoryView;
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
	textField.inputAccessoryView = [self inputAccessoryViewForIndex:textField.tag];
	return YES;
}

- (void)nextTF:(id)sender {
	NSInteger tag = ((UIBarButtonItem *)sender).tag;
	if (tag >= 0 && tag < 7) {
		NSInteger index = (tag % 10) + 1;
		UITextField *nextTF = [self.textFields objectAtIndex:index];
		[nextTF becomeFirstResponder];
	}
}

- (void)prevTF:(id)sender {
	NSInteger tag = ((UIBarButtonItem *)sender).tag;
	if (tag >= 0 && tag < 7) {
		NSInteger index = (tag % 10) - 1;
		UITextField *prevTF = [self.textFields objectAtIndex:index];
		[prevTF becomeFirstResponder];
	}
}

- (void)doneInput:(id)sender {
	[self.contentView endEditing:YES];
}

- (void)setTimeSheet:(TimeSheet *)timeSheet {
	_timeSheet = timeSheet;
	self.dataLabel.text = [timeSheet title];
	[self.contentView setNeedsLayout];
	[self.contentView layoutIfNeeded];
}

@end

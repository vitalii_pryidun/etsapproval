//
//  TSHeader.h
//  ETSApproval
//
//  Created by supAdmin on 02.11.17.
//  Copyright © 2017 SAPRun. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TSHeader : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *projectKey;
@property (weak, nonatomic) IBOutlet UILabel *projectName;

@end

//
//  TSLoadingViewController.h
//  ETSApproval
//
//  Created by supAdmin on 02.11.17.
//  Copyright © 2017 SAPRun. All rights reserved.
//

#import "EAViewController.h"

@interface TSLoadingViewController : EAViewController <TSInputSelection>

@end

//
//  TSLoadingViewController.m
//  ETSApproval
//
//  Created by supAdmin on 02.11.17.
//  Copyright © 2017 SAPRun. All rights reserved.
//

#import "TSLoadingViewController.h"
#import "CalendarViewController.h"
#import "ProjectViewController.h"
#import "TSViewController.h"
#import "Project.h"
#import "PDTSimpleCalendarViewController.h"
#import "UIView+Shake.h"

@interface TSLoadingViewController () <UITextFieldDelegate, TSInputSelection, PDTSimpleCalendarViewDelegate>

// base UI
@property (weak, nonatomic) IBOutlet UIButton *projBtn;
@property (weak, nonatomic) IBOutlet UIButton *dateBtn;
@property (weak, nonatomic) IBOutlet UIButton *loadBtn;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *loadAI;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *dateAI;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *projAI;
@property (weak, nonatomic) IBOutlet UIButton *exitBtn;
@property (weak, nonatomic) IBOutlet UIImageView *logo;
@property (weak, nonatomic) IBOutlet UIImageView *okBtnPlaceholder;

// data UI
@property (weak, nonatomic) IBOutlet UILabel *projCodeLabel;
@property (weak, nonatomic) IBOutlet UILabel *projDescriptionLabel;
@property (weak, nonatomic) IBOutlet UILabel *dateRangeLabel;
@property (weak, nonatomic) IBOutlet UIView *projContentView;
@property (weak, nonatomic) IBOutlet UIView *dateContentView;


// functional constraints
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *projBtnH;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *dateBtnH;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *projContentH;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *dateContentH;
@property (strong, nonatomic) IBOutletCollection(UILabel) NSArray *datesLabels;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *loadBtnWidth;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *okWidth;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *okHeight;

// animation constraints
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *logoMargin;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *projBtnC;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *dateBtnC;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *loadBtnTop;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *exitBtnL;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *okCenter;

// data
@property (strong, nonatomic) NSDate *date;
@property (strong, nonatomic) Project *project;
@property (strong, nonatomic) NSArray *ts;

// flags
@property (assign, nonatomic) BOOL shouldReloadDate;
@property (assign, nonatomic) BOOL shouldReloadProject;

@end

@implementation TSLoadingViewController

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)viewWillAppear:(BOOL)animated {
	[super viewWillAppear:animated];
	[self backToNormalState];
}

- (void)viewDidAppear:(BOOL)animated {
	[super viewDidAppear:animated];
	if (self.shouldReloadDate) {
		self.dateContentView.hidden = NO;
		__weak __typeof(self) weakSelf = self;
		[UIView animateWithDuration:0.35 animations:^{
			weakSelf.dateBtn.alpha = 0;
			weakSelf.dateBtnH.constant = 150;
			weakSelf.dateContentH.constant = 150;
			weakSelf.dateContentView.alpha = 0.9;
			[weakSelf.view layoutIfNeeded];
		} completion:^(BOOL finished) {
			weakSelf.dateBtn.hidden = YES;
			weakSelf.shouldReloadDate = NO;
		}];
	}
	
	if (self.shouldReloadProject) {
		self.projContentView.hidden = NO;
		__weak __typeof(self) weakSelf = self;
		[UIView animateWithDuration:0.35 animations:^{
			weakSelf.projBtn.alpha = 0;
			CGFloat emptyCellHeight = 100;
			CGFloat textWidth = weakSelf.projContentView.frame.size.width - 40;
			CGFloat textHeight = [weakSelf.project.name boundingRectWithSize:CGSizeMake(textWidth, MAXFLOAT)
															options:NSStringDrawingUsesLineFragmentOrigin
														 attributes:@{NSFontAttributeName:weakSelf.projDescriptionLabel.font} context:nil].size.height;
			CGFloat cellHeight = emptyCellHeight + textHeight;
			weakSelf.projBtnH.constant = cellHeight;
			weakSelf.projContentH.constant = cellHeight;
			weakSelf.projContentView.alpha = 0.9;
			[weakSelf.view layoutIfNeeded];
		} completion:^(BOOL finished) {
			weakSelf.projBtn.hidden = YES;
			weakSelf.shouldReloadProject = NO;
		}];
	}
}

- (IBAction)loadBtnTouched:(id)sender {
	if (self.date && self.project) {
		[self setLoadBtnLoadingState];
		
		[self.view setUserInteractionEnabled:NO];
		
		__weak __typeof(self) weakSelf = self;
		[[DataManager instance] getTSForDate:self.date andProjectKey:self.project.key success:^(NSArray *ts) {
			[weakSelf handleLoadingSuccess:ts];
		} failure:^(NSInteger errCode) {
			[weakSelf handleLoadingFailure:errCode];
		}];
	}
}

- (IBAction)anotherProjTouched:(id)sender {
	[self selectProjectBtnTouched:sender];
}

- (IBAction)selectProjectBtnTouched:(id)sender {
	[self performSegueWithIdentifier:@"project" sender:nil];
}

- (IBAction)anotherDateTouched:(id)sender {
	[self showCalendar];
}

- (IBAction)selectDateBtnTouched:(id)sender {
	[self showCalendar];
}

- (void)didSelectDate:(NSDate *)date {
	self.dateRangeLabel.text = [date stringRepresentationOfWeek];
	NSArray *days = [date daysOfWeek];
	for (NSInteger i = 0; i < 7; i++) {
		UILabel *label = self.datesLabels[i];
		label.text = days[i];
	}
	if (!self.date && date) {
		self.shouldReloadDate = YES;
	}
	self.date = date;
	[self enableLoadBtn];
}

- (void)didSelectProject:(Project *)project {
	self.projCodeLabel.text = project.key;
	self.projDescriptionLabel.text = project.name;
	
	if (self.project != project) {
		self.shouldReloadProject = YES;
	}
	self.project = project;
	[self enableLoadBtn];
}

- (void)enableLoadBtn {
	BOOL enabled = (self.date && self.project);
	self.loadBtn.enabled = enabled;
	__weak __typeof(self) weakSelf = self;
	[UIView animateWithDuration:0.35 animations:^{
		weakSelf.loadBtn.alpha = enabled ? 1 : 0.5;
	}];
}

- (void)setLoadBtnLoadingState {
	[self.loadAI startAnimating];
	[self.loadBtn setTitle:@"" forState:UIControlStateNormal];
	__weak __typeof(self) weakSelf = self;
	[UIView animateWithDuration:0.35 delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
		weakSelf.loadBtnWidth.constant = - (weakSelf.dateBtn.frame.size.width - 50);
		[weakSelf.view layoutIfNeeded];
	} completion:nil];
}

- (void)handleLoadingSuccess:(NSArray *)ts {
	[self.loadAI stopAnimating];
	[self.view setUserInteractionEnabled:YES];
	self.ts = ts;
	[self.loadBtn setTitle:@"Загрузить ТШ" forState:UIControlStateNormal];
	self.loadBtn.hidden = YES;
	self.okBtnPlaceholder.hidden = NO;
	[self performSelector:@selector(pushTSViewController) withObject:nil afterDelay:0.35];
}

- (void)pushTSViewController {
	__weak __typeof(self) weakSelf = self;
	[self fadeOutWithCompletion:^{
		[weakSelf performSegueWithIdentifier:@"ts" sender:nil];
	}];
}

- (void)backToNormalState {
	self.loadBtn.hidden = NO;
	self.okBtnPlaceholder.hidden = YES;
	[self.loadBtn setTitle:@"Загрузить ТШ" forState:UIControlStateNormal];
	self.loadBtnWidth.constant = 0;
	self.okCenter.constant = -35;
	self.exitBtnL.constant = 20;
	self.okBtnPlaceholder.alpha = 1;
	[self.view layoutIfNeeded];
}

- (void)handleLoadingFailure:(NSInteger)errCode {
	[self.loadAI stopAnimating];
	[self.view setUserInteractionEnabled:YES];
	[self alertMessage:@"Ошибка! Проверьте подключение к сети" title:nil cancelButton:nil];
	[self.loadBtn setTitle:@"Загрузить ТШ" forState:UIControlStateNormal];
	self.loadBtnWidth.constant = 0;
	[self.view layoutIfNeeded];
	[self.loadBtn shake];
}

- (void)showCalendar {
	[self performSegueWithIdentifier:@"calendar" sender:self];
}

#pragma mark - Navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
	if ([segue.identifier isEqualToString:@"project"]) {
		ProjectViewController *vc = segue.destinationViewController;
		vc.delegate = self;
	} else if ([segue.identifier isEqualToString:@"calendar"]) {
		CalendarViewController *vc = segue.destinationViewController;
		vc.delegate = self;
	} else if ([segue.identifier isEqualToString:@"ts"]) {
		TSViewController *vc = segue.destinationViewController;
		vc.project = self.project;
		vc.date = self.date;
		vc.timeSheets = self.ts;
	}
}

- (void)prepareForAnimation {
	self.logo.alpha = 0;
	self.logoMargin.constant += 200;
	
	self.projBtnC.constant = -self.view.frame.size.width;
	self.projBtn.alpha = 0;
	
	self.dateBtnC.constant = self.view.frame.size.width;
	self.dateBtn.alpha = 0;
	
	self.loadBtnTop.constant += 200;
	self.loadBtn.alpha = 0;
	
	self.exitBtn.alpha = 0;
	self.exitBtnL.constant += 200;
}

- (CGFloat)delayBeetwenAnimations {
	return 0.1;
}

- (NSArray<void (^)(void)> *)appearAnimations {
	__weak __typeof(self) weakSelf = self;
	return @[
			 	^(){
					weakSelf.projBtnC.constant = 0;
					weakSelf.projBtn.alpha = 1;
					
					weakSelf.dateBtnC.constant = 0;
					weakSelf.dateBtn.alpha = 1;
				},
				 
				 ^(){
					 weakSelf.logo.alpha = 1;
					 weakSelf.logoMargin.constant -= 200;
					 
					 weakSelf.loadBtnTop.constant -= 200;
					 weakSelf.loadBtn.alpha = 0.5;
				 },
			  
				 ^(){
					 weakSelf.exitBtn.alpha = 1;
					 weakSelf.exitBtnL.constant = 20;
				 },
			 ];
}

- (NSArray<void (^)(void)> *)fadeOutAnimations {
	__weak __typeof(self) weakSelf = self;
	return @[
			 ^(){
				 weakSelf.exitBtnL.constant = -200;
				 weakSelf.okCenter.constant = weakSelf.view.frame.size.height;
			 },
			 ];
}

- (IBAction)unwindToLoadingTS:(UIStoryboardSegue *)segue { }

@end

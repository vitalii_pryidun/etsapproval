//
//  TSUserHeaderCell.h
//  ETSApproval
//
//  Created by supAdmin on 12.02.18.
//  Copyright © 2018 SAPRun. All rights reserved.
//

#import <UIKit/UIKit.h>

@class TimeSheet;

@interface TSUserHeaderCell : UITableViewCell

@property (assign, nonatomic) BOOL expanded;
@property (strong, nonatomic) NSArray<TimeSheet *> *timeSheets;

@end

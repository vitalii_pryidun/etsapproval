//
//  TSUserHeaderCell.m
//  ETSApproval
//
//  Created by supAdmin on 12.02.18.
//  Copyright © 2018 SAPRun. All rights reserved.
//

#import "TSUserHeaderCell.h"
#import "DataManager.h"
#import "TimeSheet.h"

@interface TSUserHeaderCell ()

@property (weak, nonatomic) IBOutlet UIView *innerView;
@property (weak, nonatomic) IBOutlet UILabel *dataLabel;
@property (weak, nonatomic) IBOutlet UILabel *timeLabel;
@property (weak, nonatomic) IBOutlet UIView *separatorView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *bottomConstraint;
@property (weak, nonatomic) IBOutlet UIView *shadowContainer;
@property (strong, nonatomic) IBOutletCollection(UILabel) NSArray *dayLabels;
@property (weak, nonatomic) IBOutlet UIButton *expandBtn;

@end

@implementation TSUserHeaderCell

- (void)awakeFromNib {
    [super awakeFromNib];
	self.shadowContainer.layer.shadowColor = [UIColor blackColor].CGColor;
	self.shadowContainer.layer.shadowRadius = 8.f;
	self.shadowContainer.layer.shadowOffset = CGSizeMake(0.f, 4.f);
	self.shadowContainer.layer.shadowOpacity = 0.1f;
	self.shadowContainer.layer.masksToBounds = YES;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

- (void)setTimeSheets:(NSArray<TimeSheet *> *)timeSheets {
	_timeSheets = timeSheets;
	[self calculateSumm];
	self.dataLabel.text = timeSheets.firstObject.employee;
}

- (void)calculateSumm {
	NSMutableArray *days = [[NSMutableArray alloc] initWithArray:@[@0, @0, @0, @0, @0, @0, @0]];
	for (TimeSheet *ts in self.timeSheets) {
		for (NSInteger i = 0; i < 7; i++) {
			CGFloat day = [days[i] floatValue];
			day += [ts.days[i] floatValue];
			days[i] = @(day);
		}
	}
	
	CGFloat summ = 0.0;
	for (NSInteger i = 0; i < 7; i++) {
		UILabel *dayLabel = self.dayLabels[i];
		NSNumber *day = days[i];
		summ += [day floatValue];
		dayLabel.text = [[[DataManager instance] defaultNumberFormatter] stringFromNumber:day];
	}
	
	self.timeLabel.text = [[[[DataManager instance] defaultNumberFormatter] stringFromNumber:@(summ)] stringByAppendingString:@"ч"];
}

- (void)setExpanded:(BOOL)expanded {
	_expanded = expanded;
	self.separatorView.hidden = !expanded;
	NSString *image = expanded ? @"collapse" : @"expand";
	[self.expandBtn setImage:[UIImage imageNamed:image] forState:UIControlStateNormal];
	UIBezierPath *shadowPath;
	if (self.expanded) {
		self.bottomConstraint.constant = 0;
		shadowPath = [UIBezierPath bezierPathWithRoundedRect:self.innerView.bounds byRoundingCorners:(UIRectCornerTopLeft|UIRectCornerTopRight) cornerRadii:CGSizeMake(25.0, 25.0)];
	} else {
		self.bottomConstraint.constant = 20;
		shadowPath = [UIBezierPath bezierPathWithRoundedRect:self.innerView.bounds byRoundingCorners:(UIRectCornerBottomLeft|UIRectCornerBottomRight|UIRectCornerTopLeft|UIRectCornerTopRight) cornerRadii:CGSizeMake(25.0, 25.0)];
	}
	CAShapeLayer *maskLayer = [CAShapeLayer layer];
	maskLayer.path = shadowPath.CGPath;
	maskLayer.frame = self.innerView.bounds;
	
	self.innerView.layer.mask = maskLayer;
}

@end

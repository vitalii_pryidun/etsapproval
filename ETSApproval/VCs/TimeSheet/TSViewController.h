//
//  TSViewController.h
//  ETSApproval
//
//  Created by supAdmin on 02.11.17.
//  Copyright © 2017 SAPRun. All rights reserved.
//

#import "EAViewController.h"

@interface TSViewController : EAViewController

@property (strong, nonatomic) NSDate *date;
@property (strong, nonatomic) Project *project;
@property (strong, nonatomic) NSArray *timeSheets;

@end

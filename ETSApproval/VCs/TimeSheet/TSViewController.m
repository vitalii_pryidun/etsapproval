//
//  TSViewController.m
//  ETSApproval
//
//  Created by supAdmin on 02.11.17.
//  Copyright © 2017 SAPRun. All rights reserved.
//

#import "TSViewController.h"
#import "TimeSheet.h"
#import "Project.h"
#import "TSCell.h"
#import "TSUserHeaderCell.h"
#import "TSHeader.h"
#import "ResultViewController.h"

@interface TSViewController () <UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate>

@property (nonatomic, strong) UIToolbar *inputAccessoryView;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) NSNumberFormatter *defaultNumberFormatter;
@property (weak, nonatomic) IBOutlet UIButton *backBtn;
@property (weak, nonatomic) IBOutlet UIButton *menuBtn;
@property (weak, nonatomic) IBOutlet UIButton *loadBtn;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *loadAi;
@property (weak, nonatomic) IBOutlet UIView *customBottomBar;
@property (weak, nonatomic) IBOutlet UIView *bottomBarPlaceholderForX;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *tableViewBottom;
@property (weak, nonatomic) IBOutlet UIImageView *warningImage;

// animations
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *backBtnL;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *menuBtnT;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *bottomBarB;

// data
@property (strong, nonatomic) NSMutableDictionary *dataSource;
@property (assign, nonatomic) ResultTSAction action;

//

@property (nonatomic, strong) NSMutableArray<NSString *> *expandedUsers;

@end

@implementation TSViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
	self.defaultNumberFormatter = [[NSNumberFormatter alloc] init];
	self.defaultNumberFormatter.maximumFractionDigits = 2;
	self.defaultNumberFormatter.minimumFractionDigits = 1;
	self.defaultNumberFormatter.minimumIntegerDigits = 1;
	self.dataSource = [[self userDataForTimeSheets:self.timeSheets] mutableCopy];
	self.expandedUsers = [self.dataSource.allKeys mutableCopy];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidAppear:(BOOL)animated {
	[super viewDidAppear:animated];
	[self.tableView reloadData];
}

#pragma mark - data

- (NSDictionary *)userDataForTimeSheets:(NSArray<TimeSheet *> *)timeSheets {
	NSMutableDictionary *data = [[NSMutableDictionary alloc] init];
	NSMutableSet *users = [[NSMutableSet alloc] init];
	for (TimeSheet *ts in timeSheets) {
		[users addObject:ts.employeeKey];
	}
	
	for (NSString *user in users) {
		NSMutableArray *userTSs = [[NSMutableArray alloc] init];
		for (TimeSheet *ts in timeSheets) {
			if ([user isEqualToString:ts.employeeKey]) {
				[userTSs addObject:ts];
			}
		}
		[data setObject:userTSs forKey:user];
	}
	return data;
}

- (void)setTimeSheets:(NSArray *)timeSheets {
	_timeSheets = timeSheets;
	_dataSource = [[self userDataForTimeSheets:timeSheets] mutableCopy];
	_expandedUsers = [_dataSource.allKeys mutableCopy];
}

#pragma mark - tableview delegate

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
	NSString *user = [self userForIndexPath:indexPath];
	UITableViewCell *cell = nil;
	if (user) {
		NSArray *timeSheets = self.dataSource[user];
		TSUserHeaderCell *headerCell = [tableView dequeueReusableCellWithIdentifier:@"TSUserHeaderCell" forIndexPath:indexPath];
		headerCell.timeSheets = timeSheets;
		headerCell.expanded = [self array:self.expandedUsers containsString:user];
		cell = headerCell;
	} else {
		TSCell *tsCell = [tableView dequeueReusableCellWithIdentifier:@"TSCell" forIndexPath:indexPath];
		NSIndexPath *indexPathForHeader = [self indexPathForHeaderWithSubCellAtIndexPath:indexPath];
		NSString *user = [self userForIndexPath:indexPathForHeader];
		NSArray *timeSheets = self.dataSource[user];
		TimeSheet *ts = timeSheets[indexPath.row - indexPathForHeader.row - 1];
		tsCell.timeSheet = ts;

		BOOL isLast = timeSheets.lastObject == ts;
		tsCell.position = isLast ? PositionLast : PositionNormal;
		CGFloat summ = 0.0;
		for (int i = 0; i < 7; i++) {
			UITextField *tf = tsCell.textFields[i];
			tf.text = [self.defaultNumberFormatter stringFromNumber:ts.days[i]];
			tf.tag = i + indexPath.row * 10;
			tf.delegate = self;
			
			summ += [ts.days[i] floatValue];
		}
		tsCell.timeLabel.text = [[[self defaultNumberFormatter] stringFromNumber:@(summ)] stringByAppendingString:@"ч"];
		cell = tsCell;
	}
	
	cell.contentView.backgroundColor = [UIColor clearColor];
	cell.backgroundColor = cell.contentView.backgroundColor;
	cell.selectionStyle = UITableViewCellSelectionStyleNone;
	return cell;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
	NSString *user = [self userForIndexPath:indexPath];
	if (user) {
		((TSUserHeaderCell *)cell).expanded = [self array:self.expandedUsers containsString:user];
	} else {
		NSIndexPath *indexPathForHeader = [self indexPathForHeaderWithSubCellAtIndexPath:indexPath];
		NSString *user = [self userForIndexPath:indexPathForHeader];
		NSArray *timeSheets = self.dataSource[user];
		TimeSheet *ts = timeSheets[indexPath.row - indexPathForHeader.row - 1];
		
		BOOL isLast = timeSheets.lastObject == ts;
		((TSCell *)cell).position = isLast ? PositionLast : PositionNormal;
	}
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
	return UITableViewAutomaticDimension;
}

- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath {
	return UITableViewAutomaticDimension;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
	return 150;
}

- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForHeaderInSection:(NSInteger)section {
	return 150;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
	TSHeader *header = [tableView dequeueReusableCellWithIdentifier:@"TSHeader"];
	header.projectKey.text = self.project.key;
	header.projectName.text = self.project.name;
	return header.contentView;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
	return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
	if (!self.timeSheets || self.timeSheets.count == 0) {
		[self.tableView setHidden:YES];
		[self.warningImage setHidden:NO];
		[self showErrorLabel:@"Данные отсутствуют"];
	} else {
		[self.tableView setHidden:NO];
		[self.warningImage setHidden:YES];
		[self hideErrorLabel];
	}
	NSArray *users = self.dataSource.allKeys;
	NSInteger numberOfRows = users.count;
	for (NSString *user in users) {
		if ([self array:self.expandedUsers containsString:user]) {
			numberOfRows += ((NSArray *) self.dataSource[user]).count;
		}
	}
	return numberOfRows;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
	if ([[tableView cellForRowAtIndexPath:indexPath].reuseIdentifier isEqualToString:@"TSUserHeaderCell"]) {
		NSString *user = [self userForIndexPath:indexPath];
		NSArray *timeSheets = self.dataSource[user];
		NSInteger index = indexPath.row;
		NSMutableArray *indexPaths = [[NSMutableArray alloc] init];
		for (NSInteger i = 0; i < timeSheets.count; i++) {
			index++;
			[indexPaths addObject:[NSIndexPath indexPathForRow:index inSection:0]];
		}
		if ([self array:self.expandedUsers containsString:user]) {
			[self.expandedUsers removeObject:user];
			[self.tableView beginUpdates];
			[self.tableView deleteRowsAtIndexPaths:indexPaths withRowAnimation:UITableViewRowAnimationFade];
			[self.tableView endUpdates];
		} else {
			[self.expandedUsers addObject:user];
			[self.tableView beginUpdates];
			[self.tableView insertRowsAtIndexPaths:indexPaths withRowAnimation:UITableViewRowAnimationFade];
			[self.tableView endUpdates];
		}
		[self.tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
	}
}

- (BOOL)array:(NSArray<NSString *> * _Nonnull )array containsString:(NSString *)string {
	for (NSString *arrayString in array) {
		if ([arrayString isEqualToString:string]) {
			return YES;
		}
	}
	return NO;
}

- (NSIndexPath *)indexPathForHeaderWithSubCellAtIndexPath:(NSIndexPath *)indexPath {
	NSIndexPath *headerIndexPath = nil;
	NSArray *users = self.dataSource.allKeys;
	NSIndexPath *previousPath = nil;
	for (NSString *user in users) {
		NSIndexPath *indexToCheck = [self indexPathForUser:user];
		if (indexToCheck.row >= indexPath.row) {
			headerIndexPath = previousPath;
			break;
		} else {
			previousPath = indexToCheck;
		}
	}
	headerIndexPath = previousPath;
	return headerIndexPath;
}

- (NSString *)userForIndexPath:(NSIndexPath *)indexPath {
	NSArray *users = self.dataSource.allKeys;
	NSString *user = nil;
	NSInteger index = 0;
	for (NSString *userKey in users) {
		if (indexPath.row == index) {
			user = userKey;
		}
		
		BOOL isExpanded = [self array:self.expandedUsers containsString:userKey];
		index += isExpanded ? ((NSArray *)self.dataSource[userKey]).count : 0;
		index++;
	}
	return user;
}

- (NSIndexPath *)indexPathForUser:(NSString *)user {
	NSArray *users = self.dataSource.allKeys;
	NSIndexPath *indexPath = nil;
	if ([users containsObject:user]) {
		NSInteger index = 0;
		for (NSString *userKey in users) {
			if ([user isEqualToString:userKey]) {
				indexPath = [NSIndexPath indexPathForRow:index inSection:0];
				break;
			}
			index++;
			BOOL isExpanded = [self array:self.expandedUsers containsString:userKey];
			index += isExpanded ? ((NSArray *)self.dataSource[userKey]).count : 0;
		}
	}
	return indexPath;
}

#pragma mark - textfield delegate

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
	if ([textField.text isEqualToString:[self.defaultNumberFormatter stringFromNumber:@(0)]]) {
		textField.text = @"";
	}
	return YES;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
	
	NSString *localDeciamalSeparator = self.defaultNumberFormatter.decimalSeparator;
	NSString *resultString = [textField.text stringByReplacingCharactersInRange:range withString:string];
	
	return !([textField.text containsString:localDeciamalSeparator] && [string containsString:localDeciamalSeparator]) && [resultString rangeOfString:localDeciamalSeparator].location != 0;
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
	NSInteger row = textField.tag / 10;
	NSInteger day = textField.tag % 10;
	NSIndexPath *indexPath = [NSIndexPath indexPathForRow:row inSection:0];
	NSIndexPath *headerPath = [self indexPathForHeaderWithSubCellAtIndexPath:indexPath];
	NSString *user = [self userForIndexPath:headerPath];
	NSMutableArray *timeSheets = [self.dataSource[user] mutableCopy];
	NSInteger index = row - headerPath.row - 1;
	TimeSheet *ts = [timeSheets objectAtIndex:index];
	NSNumber *value = [self.defaultNumberFormatter numberFromString:textField.text];
	if (!value) {
		value = @(0);
	}
	ts.days[day] = value;
	timeSheets[index] = ts;
	[self.dataSource setObject:timeSheets forKey:user];
	textField.text = [self.defaultNumberFormatter stringFromNumber:value];
	[textField resignFirstResponder];
	[textField layoutIfNeeded];
	
	[self.tableView reloadRowsAtIndexPaths:@[indexPath, headerPath] withRowAnimation:UITableViewRowAnimationNone];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
	[textField resignFirstResponder];
	return YES;
}

- (void)keyboardDidShow:(NSNotification *)notification {
	CGSize keyboardSize = [[[notification userInfo] objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].size;
	__weak typeof(self) weakSelf = self;
	[UIView animateWithDuration:0.1 animations:^{
		CGFloat bottomBarHeight = weakSelf.view.frame.size.height - weakSelf.customBottomBar.frame.origin.y;
		weakSelf.tableViewBottom.constant = keyboardSize.height - bottomBarHeight;
		[weakSelf.view layoutIfNeeded];
	}];
}

- (void)keyboardDidHide:(NSNotification *)notification {
	__weak typeof(self) weakSelf = self;
	[UIView animateWithDuration:0.1 animations:^{
		weakSelf.tableViewBottom.constant = 0;
		[weakSelf.view layoutIfNeeded];
	}];
}

#pragma mark - approving

- (IBAction)menuBtnTouched:(id)sender {
	__weak __typeof(self) weakSelf = self;
	UIAlertController *actionSheet = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
	actionSheet.popoverPresentationController.sourceView = sender;
	actionSheet.popoverPresentationController.sourceRect = CGRectMake(44, 0, 1, 1);
	actionSheet.popoverPresentationController.permittedArrowDirections = UIPopoverArrowDirectionUnknown;
	[actionSheet addAction:[UIAlertAction actionWithTitle:@"Сохранить" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
		[weakSelf saveTS];
		[actionSheet dismissViewControllerAnimated:YES completion:nil];
	}]];
	[actionSheet addAction:[UIAlertAction actionWithTitle:@"Утвердить" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
		[weakSelf approveTS];
		[actionSheet dismissViewControllerAnimated:YES completion:nil];
	}]];
	[actionSheet addAction:[UIAlertAction actionWithTitle:@"Удалить" style:UIAlertActionStyleDestructive handler:^(UIAlertAction * _Nonnull action) {
		[weakSelf deleteTS];
		[actionSheet dismissViewControllerAnimated:YES completion:nil];
	}]];
	[actionSheet addAction:[UIAlertAction actionWithTitle:@"Отмена" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
		[actionSheet dismissViewControllerAnimated:YES completion:nil];
	}]];
	[self presentViewController:actionSheet animated:YES completion:nil];
}

#pragma mark - navigation

- (IBAction)backBtnTouched:(id)sender {
	[self.navigationController popViewControllerAnimated:YES];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
	if ([segue.identifier isEqualToString:@"send"]) {
		ResultViewController *vc = segue.destinationViewController;
		vc.action = self.action;
		vc.ts = [self timeSheetsForBackend];
		vc.date = self.date;
		vc.projectKey = self.project.key;
	}
}

- (void)saveTS {
	self.action = save;
	__weak __typeof(self) weakSelf = self;
	[self fadeOutWithCompletion:^{
		[weakSelf performSegueWithIdentifier:@"send" sender:self];
	}];
}

- (void)approveTS {
	self.action = approve;
	__weak __typeof(self) weakSelf = self;
	[self fadeOutWithCompletion:^{
		[weakSelf performSegueWithIdentifier:@"send" sender:self];
	}];
}

- (void)deleteTS {
	self.action = clear;
	__weak __typeof(self) weakSelf = self;
	[self fadeOutWithCompletion:^{
		[weakSelf performSegueWithIdentifier:@"send" sender:self];
	}];
}

- (NSArray *)timeSheetsForBackend {
	NSMutableArray *tsInBackendFormats = [[NSMutableArray alloc] init];
	for (NSString *user in self.dataSource) {
		NSArray *timeSheets = self.dataSource[user];
		for (TimeSheet *ts in timeSheets) {
			[tsInBackendFormats addObject:[ts arrayRepresentation]];
		}
	}
	
	return tsInBackendFormats;
}

#pragma mark - bottombar buttons

- (IBAction)loadTSBtnTouched:(id)sender {
	[self.view setUserInteractionEnabled:NO];
	[self.loadAi startAnimating];
	self.loadBtn.alpha = 0.5;
	__weak __typeof(self) weakSelf = self;
	[[DataManager instance] reloadTSForDate:self.date andProjectKey:self.project.key success:^(NSArray * ts) {
		weakSelf.timeSheets = ts;
		weakSelf.tableView.hidden = NO;
		[weakSelf.tableView reloadData];
		[weakSelf.view setUserInteractionEnabled:YES];
		[weakSelf.loadAi stopAnimating];
		weakSelf.loadBtn.alpha = 1;
	} failure:^(NSInteger errCode) {
		weakSelf.timeSheets = @[];
		weakSelf.tableView.hidden = YES;
		[weakSelf showErrorLabel:@"Не удалось загрузить данные.\nПроверьте подключение к сети"];
		[weakSelf.view setUserInteractionEnabled:YES];
		[weakSelf.loadAi stopAnimating];
		weakSelf.loadBtn.alpha = 1;
	}];
}

- (IBAction)approveBtnTouched:(id)sender {
	[self approveTS];
}

#pragma mark - animations

- (void)prepareForAnimation {
	self.backBtnL.constant += 40;
	self.backBtn.alpha = 0;
	self.customBottomBar.alpha = 0;
	self.bottomBarPlaceholderForX.alpha = 0;
	self.bottomBarB.constant -= 100;
	self.warningImage.alpha = 0;
}

- (NSArray<void (^)(void)> *)appearAnimations {
	__weak __typeof(self) weakSelf = self;
	return @[ ^(){
		weakSelf.backBtnL.constant -= 40;
		weakSelf.backBtn.alpha = 1;
		weakSelf.customBottomBar.alpha = 1;
		weakSelf.bottomBarPlaceholderForX.alpha = 1;
		weakSelf.bottomBarB.constant += 100;
		weakSelf.warningImage.alpha = 1;
	} ];
}

- (NSArray<void (^)(void)> *)fadeOutAnimations {
	__weak __typeof(self) weakSelf = self;
	return @[ ^(){
		weakSelf.backBtnL.constant -= 40;
		weakSelf.backBtn.alpha = 0;
		weakSelf.customBottomBar.alpha = 0;
		weakSelf.bottomBarPlaceholderForX.alpha = 0;
		weakSelf.bottomBarB.constant -= 100;
		weakSelf.tableView.alpha = 0;
		weakSelf.warningImage.alpha = 0;
	} ];
}

- (void)backToNormalState {
	self.backBtnL.constant += 40;
	self.backBtn.alpha = 1;
	self.customBottomBar.alpha = 1;
	self.bottomBarPlaceholderForX.alpha = 1;
	self.bottomBarB.constant += 100;
	self.tableView.alpha = 1;
	self.warningImage.alpha = 1;
	[self.view layoutIfNeeded];
}

@end

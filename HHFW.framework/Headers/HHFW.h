//
//  HHFW.h
//  HHFW
//
//  Created by max on 15/04/16.
//  Copyright © 2016 EigenMethod. All rights reserved.
//
//  HyperHive framework

#import <HHFW/HHFWController.h>
#import <HHFW/HHFWSchedule.h>
#import <HHFW/HHFWConfiguration.h>
#import <HHFW/RequestCallParams.h>
#import <HHFW/WebCallParams.h>
#import <HHFW/DeltaStreamCallParams.h>
#import <HHFW/TableCallParams.h>
#import <HHFW/DeltaCallParams.h>
#import <HHFW/WebDavCallParams.h>

//! Project version number for HHFW.
FOUNDATION_EXPORT double HHFWVersionNumber;

//! Project version string for HHFW.
FOUNDATION_EXPORT const unsigned char HHFWVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <HHFW/PublicHeader.h>



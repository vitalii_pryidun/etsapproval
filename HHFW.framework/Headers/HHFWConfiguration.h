//
//  HHFWConfiguration.h
//  HHFW
//
//  Created by max on 30/08/16.
//  Copyright © 2016 EigenMethod. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface HHFWConfiguration : NSObject

@property (nonatomic, strong, nonnull) NSString *userFolder;

+ (instancetype _Nonnull)sharedInstance;

@end

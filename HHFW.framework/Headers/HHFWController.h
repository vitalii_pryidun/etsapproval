//
//  CppDelegate.h
//  ios-native
//
//  Created by Максим Дорошенко on 29.03.16.
//  Copyright © 2016 Max Doroshenko. All rights reserved.
//
//  Main CppRun wrapper

#import <Foundation/Foundation.h>
#import "RequestCallParams.h"
#import "WebCallParams.h"
#import "DeltaStreamCallParams.h"
#import "TableCallParams.h"
#import "DeltaCallParams.h"
#import "WebDavCallParams.h"

@interface HHFWController : NSObject

typedef void (^HHFWCompletionHandler)(id _Nullable jsonResult);

+ (instancetype _Nonnull)sharedInstance;

@property (nonatomic, strong, nonnull) NSString *api;
@property (nonatomic, strong, nonnull) NSString *host;
@property (nonatomic, strong, nonnull) NSString *evvironment;
@property (nonatomic, strong, nonnull) NSString *project;
@property (nonatomic, strong, nonnull) NSString *app_name;
@property (nonatomic, strong) NSString * _Nonnull deviceID;

- (void)initWithCredentials:(NSString *_Nonnull)api
                       host:(NSString *_Nonnull)host
                environment:(NSString *_Nonnull)env
                    project:(NSString *_Nonnull)prj
                application:(NSString *_Nonnull)app
                     device:(NSString *_Nonnull)device;

#pragma mark - auth methods

- (void)auth:(NSString *_Nonnull)login
    password:(NSString *_Nonnull)password
     handler:(HHFWCompletionHandler _Nullable)handler;

- (void)resources:(NSString *_Nullable)database
          handler:(HHFWCompletionHandler _Nullable)handler;

- (void)readCookie:(HHFWCompletionHandler _Nullable)handler;

- (void)unAuth;

#pragma mark - request methods

- (void)request:(NSString *_Nonnull)resourseName
requestCallParams:(RequestCallParams *_Nullable)requestCallParams
        handler:(HHFWCompletionHandler _Nullable)handler;

- (void)deltaStream:(NSString *_Nonnull)resourseName
deltaStreamCallParams:(DeltaStreamCallParams *_Nullable)deltaStreamCallParams
    handler:(HHFWCompletionHandler _Nullable)handler;

- (void)table:(NSString *_Nonnull)resourseName
tableCallParams:(TableCallParams *_Nullable)tableCallParams
    handler:(HHFWCompletionHandler _Nullable)handler;

#pragma mark - webdav methods

- (void)put:(NSString * _Nonnull)filename
   resource:(NSString * _Nonnull)resource
webDavCallParams:(WebDavCallParams * _Nullable)webDavCallParams
completionHandler:(HHFWCompletionHandler _Nullable)handler;

- (void)get:(NSString * _Nonnull)filename
   resource:(NSString * _Nonnull)resource
webDavCallParams:(WebDavCallParams * _Nullable)webDavCallParams
completionHandler:(HHFWCompletionHandler _Nullable)handler;

- (void)delete:(NSString * _Nonnull)resource
webDavCallParams:(WebDavCallParams * _Nullable)webDavCallParams
completionHandler:(HHFWCompletionHandler _Nullable)handler;

- (void)mkColl:(NSString * _Nonnull)resource
webDavCallParams:(WebDavCallParams * _Nullable)webDavCallParams
completionHandler:(HHFWCompletionHandler _Nullable)handler;

- (void)propFind:(NSString * _Nonnull)resource
webDavCallParams:(WebDavCallParams * _Nullable)webDavCallParams
completionHandler:(HHFWCompletionHandler _Nullable)handler;

- (void)options:(NSString * _Nonnull)resource
webDavCallParams:(WebDavCallParams * _Nullable)webDavCallParams
completionHandler:(HHFWCompletionHandler _Nullable)handler;

- (void)head:(NSString * _Nonnull)resource
webDavCallParams:(WebDavCallParams * _Nullable)webDavCallParams
completionHandler:(HHFWCompletionHandler _Nullable)handler;

#pragma mark - database methods

- (BOOL)openBase:(NSString * _Nonnull)pathBase key:(NSString * _Nonnull)key;

- (BOOL)closeBase:(NSString * _Nonnull)pathBase;

- (NSString *_Nullable)query:(NSString *_Nullable)base query:(NSString *_Nonnull)query;

#pragma mark - state methods

- (NSString *_Nullable)state;

- (bool)setState:(NSString *_Nonnull)state;


#pragma mark - logging methods

- (void)setLogLevel:(int)level;

- (void)logTrace:(NSString * _Nullable)msg;
- (void)logWarning:(NSString * _Nullable)msg;
- (void)logFatal:(NSString * _Nullable)msg;


#pragma mark - Old and other method

// Abort operation
- (void)abortCurrentOperation;


// SSL checks
- (void)setSLLChecks:(BOOL)bEnabled;


// Set sqlite crypt key
- (void)setKey:(NSString * _Nullable)key;

// Remove all sqlite files
- (void)dropCache;

#pragma mark - Push notification methods

- (void)getTokens:(HHFWCompletionHandler _Nullable)handler;

- (void)addToken:(NSString *_Nonnull)token
         service:(NSString *)service
completionHandler:(HHFWCompletionHandler _Nullable)handler;

- (void)removeTokens:(NSMutableArray *_Nonnull)tokens
   completionHandler:(HHFWCompletionHandler _Nullable)handler;

- (void)getTopics:(HHFWCompletionHandler _Nullable)handler;

- (void)subscribeTopics:(NSMutableArray *_Nonnull)topics
      completionHandler:(HHFWCompletionHandler _Nullable)handler;

- (void)unsubscribeTopics:(NSMutableArray *_Nonnull)topics
        completionHandler:(HHFWCompletionHandler _Nullable)handler;


#pragma mark - Logging
- (void)setLogsKey:(NSString * _Nullable)key;
- (bool)performLogging:(NSString * _Nullable)dbName;

- (void)syncLogsShedule:(NSString * _Nonnull)url
            completionHandler:(HHFWCompletionHandler _Nullable)handler;

- (void)syncLogs:(NSString * _Nonnull)url
             makeClean:(BOOL)makeClean
     completionHandler:(HHFWCompletionHandler _Nullable)handler;


@end

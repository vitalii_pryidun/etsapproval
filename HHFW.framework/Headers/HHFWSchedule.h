//
//  HHFWSchedule.h
//  HHFW
//
//  Created by max on 30/09/16.
//  Copyright © 2016 EigenMethod. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface HHFWSchedule : NSObject

- (void)getSchedule:(NSString * _Nonnull)url completionHandler:(void(^ _Nullable)())handler;
- (void)checkSchedule:(NSString * _Nonnull)url completionHandler:(void(^ _Nullable)())handler;

@end

//
//  RequestCallParams.h
//  HHFW
//
//  Created by Dima Treefonov on 13.07.17.
//  Copyright © 2017 EigenMethod. All rights reserved.
//


#import <Foundation/Foundation.h>

@interface RequestCallParams : NSObject

@property (nonatomic, strong) NSString *data;
@property (nonatomic, strong) NSString *dataBasePath;
@property (nonatomic) int *version;
@property (nonatomic, strong) NSDictionary *headers;
@property (nonatomic) NSInteger *retryCount;
@property (nonatomic) NSInteger *retryIntervalSec;
@property (nonatomic, strong) NSString *fileName;
@property (nonatomic, strong) NSDictionary *args;

- (void)initWithDefaultProperty;

- (void)initWithDefaultDb;

@end

//
//  WebCallParams.h
//  HHFW
//
//  Created by Dima Treefonov on 17.07.17.
//  Copyright © 2017 EigenMethod. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface WebCallParams : NSObject

@property (nonatomic, strong) NSString *data;
@property (nonatomic, strong) NSDictionary *headers;
@property (nonatomic) int *retryCount;
@property (nonatomic) int *retryIntervalSec;
@property (nonatomic, strong) NSString *fileName;

- (void)initWithDefaultProperty;

@end

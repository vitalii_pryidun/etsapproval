//
//  WebDavCallParams.h
//  HHFW
//
//  Created by Dima Treefonov on 18.07.17.
//  Copyright © 2017 EigenMethod. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface WebDavCallParams : NSObject

@property (nonatomic, strong) NSString *data;
@property (nonatomic, strong) NSDictionary *headers;
@property (nonatomic) int retryCount;
@property (nonatomic) int interval;

- (void)initWithDefaultProperty;

@end
